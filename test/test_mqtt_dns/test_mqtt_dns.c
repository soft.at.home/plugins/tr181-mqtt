/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include "mqtt.h"
#include "dm_mqtt.h"
#include "mqtt_interface.h"

#include "mock.h"
#include "mosquitto_mock.h"
#include "test_mqtt_dns.h"
#include "test_common.h"

static amxo_parser_t parser;
static const char* odl_defs = "../../odl/tr181-mqtt_definition.odl";

static void object_changed_cb(UNUSED const char* const sig_name,
                              const amxc_var_t* const data,
                              UNUSED void* const priv) {
    static int num_events = 0;
    const char* from = GETP_CHAR(data, "parameters.Status.from");
    const char* to = GETP_CHAR(data, "parameters.Status.to");

    amxc_var_dump(data, 1);
    if(num_events == 0) {
        assert_string_equal(from, "Connecting");
        assert_string_equal(to, "Error_BrokerUnreachable");
    } else if(num_events == 1) {
        assert_string_equal(from, "Error_BrokerUnreachable");
        assert_string_equal(to, "Connecting");
    } else if(num_events == 2) {
        assert_string_equal(from, "Connecting");
        assert_string_equal(to, "Connected");
    }

    num_events++;
}

int test_mqtt_setup(UNUSED void** state) {
    amxd_object_t* root_obj = NULL;
    amxd_dm_t* dm = mock_init_dm();
    int retval = 0;

    root_obj = amxd_dm_get_root(dm);
    assert_non_null(root_obj);

    amxo_resolver_ftab_add(&parser, "mqtt_instance_is_valid", AMXO_FUNC(_mqtt_instance_is_valid));
    amxo_resolver_ftab_add(&parser, "mqtt_instance_cleanup", AMXO_FUNC(_mqtt_instance_cleanup));

    amxo_resolver_ftab_add(&parser, "mqtt_client_toggled", AMXO_FUNC(_mqtt_client_toggled));
    amxo_resolver_ftab_add(&parser, "mqtt_client_disabled", AMXO_FUNC(_mqtt_client_disabled));
    amxo_resolver_ftab_add(&parser, "mqtt_client_added", AMXO_FUNC(_mqtt_client_added));
    amxo_resolver_ftab_add(&parser, "mqtt_client_auto_reconnect", AMXO_FUNC(_mqtt_client_auto_reconnect));

    amxo_resolver_ftab_add(&parser, "print_event", AMXO_FUNC(_print_event));

    retval = amxo_parser_parse_file(&parser, odl_defs, root_obj);
    assert_int_equal(retval, 0);

    capture_sigalrm();
    handle_events();

    will_return(__wrap_amxb_listen, -1); // setup direct pcb listen socket
    assert_int_equal(_mqtt_main(0, dm, &parser), 0);

    return 0;
}

int test_mqtt_teardown(UNUSED void** state) {
    amxd_dm_t* dm = mock_get_dm();

    assert_int_equal(_mqtt_main(1, dm, &parser), 0);
    expect_any_count(__wrap_mosquitto_destroy, mosq, 1);

    amxo_parser_clean(&parser);
    mock_cleanup_dm();

    return 0;
}

void test_dns_resolution_can_fail(UNUSED void** state) {
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t* params = NULL;
    const char* path = "MQTT.Client.";
    amxd_object_t* client = amxd_dm_findf(mock_get_dm(), path);
    int num_cons_before = 0;
    int num_cons_after = 0;
    char* status = NULL;
    amxd_dm_t* dm = mock_get_dm();
    const char* name = "client-test";

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    params = amxc_var_add_key(amxc_htable_t, &args, "parameters", NULL);
    amxc_var_add_key(bool, params, "Enable", true);
    amxc_var_add_key(cstring_t, params, "BrokerAddress", "broker.hivemq.com");
    amxc_var_add_key(cstring_t, params, "ClientID", name);
    amxc_var_add_key(cstring_t, params, "Name", name);
    amxc_var_add_key(cstring_t, params, "ProtocolVersion", "5.0");
    assert_int_equal(amxd_object_invoke_function(client, "_add", &args, &ret), 0);

    amxc_var_dump(&ret, STDOUT_FILENO);
    client = amxd_dm_findf(mock_get_dm(), "MQTT.Client.%d", GET_UINT32(&ret, "index"));
    assert_non_null(client);
    client = amxd_dm_findf(mock_get_dm(), "MQTT.Client.%s", GET_CHAR(&ret, "name"));
    assert_non_null(client);

    expect_any(__wrap_mosquitto_new, id);
    expect_any(__wrap_mosquitto_new, clean_session);
    expect_any(__wrap_mosquitto_new, obj);
    will_return(__wrap_mosquitto_new, true);

    expect_any(__wrap_mosquitto_int_option, mosq);
    expect_value(__wrap_mosquitto_int_option, option, MOSQ_OPT_PROTOCOL_VERSION);
    expect_value(__wrap_mosquitto_int_option, value, MQTT_PROTOCOL_V5);

    // DNS resolving could fail
    will_return(__wrap_ares_gethostbyname, -1);

    // Check that number of connections before ares fails is the same as after
    num_cons_before = amxc_llist_size(amxo_parser_get_connections(&parser));
    printf("Number of amxo connections is: %d\n", num_cons_before);
    fflush(stdout);
    handle_events();

    num_cons_after = amxc_llist_size(amxo_parser_get_connections(&parser));
    printf("Number of amxo connections is: %d\n", num_cons_after);
    fflush(stdout);
    assert_int_equal(num_cons_before, num_cons_after);

    // Status should be Connecting
    status = amxd_object_get_value(cstring_t, client, "Status", NULL);
    assert_string_equal(status, "Connecting");
    free(status);

    // wait for retry mechanism to pop in and let DNS resolving succeed
    will_return(__wrap_ares_gethostbyname, 0);
    expect_any(__wrap_mosquitto_connect_bind_async_v5, mosq);
    expect_string(__wrap_mosquitto_connect_bind_async_v5, host, "broker.hivemq.com");
    expect_value(__wrap_mosquitto_connect_bind_async_v5, port, 1883);
    expect_value(__wrap_mosquitto_connect_bind_async_v5, keepalive, 60);
    expect_any(__wrap_mosquitto_connect_bind_async_v5, bind_address);
    expect_any(__wrap_mosquitto_connect_bind_async_v5, properties);
    will_return(__wrap_mosquitto_connect_bind_async_v5, 0);

    handle_events();
    read_sigalrm();
    amxp_timers_calculate();
    amxp_timers_check();

    mqtt_ctrl_handle_item(mqtt_ctrl_fd(), NULL);

    // Subscribe to events, because several Status changes will happen in the next handle_events
    assert_int_equal(amxp_slot_connect(&dm->sigmngr, "dm:object-changed", NULL, object_changed_cb, dm), 0);
    handle_events();

    assert_int_equal(amxp_slot_disconnect_with_priv(&dm->sigmngr, object_changed_cb, dm), 0);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    assert_int_equal(amxd_object_invoke_function(client, "_get", &args, &ret), 0);
    amxc_var_dump(&ret, STDOUT_FILENO);

    params = amxc_var_get_key(&ret, "MQTT.Client.1.", AMXC_VAR_FLAG_DEFAULT);
    assert_string_equal(GET_CHAR(params, "Status"), "Connected");
    assert_string_equal(GET_CHAR(params, "ProtocolVersion"), "5.0");

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}
