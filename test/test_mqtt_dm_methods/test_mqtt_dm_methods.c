/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include "mqtt.h"
#include "dm_mqtt.h"
#include "mqtt_interface.h"

#include "mock.h"
#include "amxb_mock.h"
#include "imtp_mock.h"
#include "test_mqtt_dm_methods.h"
#include "test_common.h"

static amxo_parser_t parser;
static const char* odl_defs = "../../odl/tr181-mqtt_definition.odl";
static int listen_fd = 123;
static int accept_fd_1 = 456;
static int accept_fd_2 = 789;

static amxd_object_t* add_enabled_client(void) {
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t* params = NULL;
    amxd_object_t* client = amxd_dm_findf(mock_get_dm(), "MQTT.Client");
    amxc_string_t name;
    static int index = 0;

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_string_init(&name, 0);

    index++;
    amxc_string_setf(&name, "MQTT-Client-%d", index);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    params = amxc_var_add_key(amxc_htable_t, &args, "parameters", NULL);
    amxc_var_add_key(cstring_t, params, "BrokerAddress", "broker.hivemq.com");
    amxc_var_add_key(cstring_t, params, "Name", amxc_string_get(&name, 0));
    amxc_var_add_key(bool, params, "Enable", true);
    assert_int_equal(amxd_object_invoke_function(client, "_add", &args, &ret), 0);
    amxc_var_dump(&ret, STDOUT_FILENO);

    expect_any(__wrap_mosquitto_new, id);
    expect_any(__wrap_mosquitto_new, clean_session);
    expect_any(__wrap_mosquitto_new, obj);
    will_return(__wrap_mosquitto_new, true);

    expect_any(__wrap_mosquitto_int_option, mosq);
    expect_value(__wrap_mosquitto_int_option, option, MOSQ_OPT_PROTOCOL_VERSION);
    expect_value(__wrap_mosquitto_int_option, value, MQTT_PROTOCOL_V5);

    will_return(__wrap_ares_gethostbyname, 0);
    expect_any(__wrap_mosquitto_connect_bind_async_v5, mosq);
    expect_string(__wrap_mosquitto_connect_bind_async_v5, host, "broker.hivemq.com");
    expect_value(__wrap_mosquitto_connect_bind_async_v5, port, 1883);
    expect_value(__wrap_mosquitto_connect_bind_async_v5, keepalive, 60);
    expect_any(__wrap_mosquitto_connect_bind_async_v5, bind_address);
    expect_any(__wrap_mosquitto_connect_bind_async_v5, properties);
    will_return(__wrap_mosquitto_connect_bind_async_v5, 0);

    handle_events();
    read_sigalrm();
    amxp_timers_calculate();
    amxp_timers_check();
    mqtt_ctrl_handle_item(mqtt_ctrl_fd(), NULL); // mqtt_client_connected

    client = amxd_dm_findf(mock_get_dm(), "MQTT.Client.%s", GET_CHAR(&ret, "name"));
    assert_non_null(client);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    assert_int_equal(amxd_object_invoke_function(client, "_get", &args, &ret), 0);
    amxc_var_dump(&ret, STDOUT_FILENO);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    amxc_string_clean(&name);

    return client;
}

int test_mqtt_setup(UNUSED void** state) {
    amxd_object_t* root_obj = NULL;
    amxd_dm_t* dm = mock_init_dm();

    assert_int_equal(amxo_parser_init(&parser), 0);

    root_obj = amxd_dm_get_root(dm);
    assert_non_null(root_obj);

    amxo_resolver_ftab_add(&parser, "mqtt_instance_is_valid", AMXO_FUNC(_mqtt_instance_is_valid));
    amxo_resolver_ftab_add(&parser, "mqtt_instance_cleanup", AMXO_FUNC(_mqtt_instance_cleanup));

    amxo_resolver_ftab_add(&parser, "mqtt_client_toggled", AMXO_FUNC(_mqtt_client_toggled));
    amxo_resolver_ftab_add(&parser, "mqtt_client_disabled", AMXO_FUNC(_mqtt_client_disabled));
    amxo_resolver_ftab_add(&parser, "mqtt_client_added", AMXO_FUNC(_mqtt_client_added));
    amxo_resolver_ftab_add(&parser, "mqtt_client_auto_reconnect", AMXO_FUNC(_mqtt_client_auto_reconnect));

    amxo_resolver_ftab_add(&parser, "print_event", AMXO_FUNC(_print_event));

    assert_int_equal(amxo_parser_parse_file(&parser, odl_defs, root_obj), 0);

    capture_sigalrm();
    handle_events();

    will_return(__wrap_amxb_listen, -1); // setup direct pcb listen socket
    assert_int_equal(_mqtt_main(0, dm, &parser), 0);

    // Create one MQTT.Client. instance
    add_enabled_client();

    return 0;
}

int test_mqtt_teardown(UNUSED void** state) {
    amxd_dm_t* dm = mock_get_dm();

    handle_events();

    assert_int_equal(_mqtt_main(1, dm, &parser), 0);

    amxo_parser_clean(&parser);

    // mqtt_imtp_connection_clean_up
    // Connection 1 has no listen ctx (cleaned after accepting) and no accepted ctx (cleaned after invalid read)
    // Connection 2 has no listen ctx (cleaned after accepting), but does have accepted ctx
    will_return(__wrap_imtp_connection_get_fd, accept_fd_2);
    expect_any_count(__wrap_mosquitto_destroy, mosq, 1);
    mock_cleanup_dm();                    // will trigger mqtt_instance_cleanup

    return 0;
}

void test_client_create_listen_socket(UNUSED void** state) {
    amxc_var_t args;
    amxc_var_t ret;
    amxd_object_t* object = NULL;

    amxc_var_init(&ret);
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "uri", "usp:/tmp/test.sock");

    object = amxd_dm_findf(mock_get_dm(), "MQTT.Client.1.");
    assert_non_null(object);

    will_return(__wrap_imtp_connection_get_fd, listen_fd);
    assert_int_equal(_Client_CreateListenSocket(object, NULL, &args, &ret), 0);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_can_accept_socket_connection(UNUSED void** state) {
    mqtt_client_t* cl = NULL;
    amxd_object_t* client = amxd_dm_findf(mock_get_dm(), "MQTT.Client.1.");
    amxo_connection_t* connection = NULL;
    mqtt_imtp_t* imtp = NULL;

    assert_non_null(client);
    assert_non_null(client->priv);

    cl = (mqtt_client_t*) client->priv;
    assert_non_null(cl);
    imtp = (mqtt_imtp_t*) amxc_llist_it_get_data(amxc_llist_get_first(&cl->imtp_cons), mqtt_imtp_t, it);
    assert_non_null(imtp);

    connection = amxo_connection_get_first(mqtt_get_parser(), CON_LISTEN);
    assert_non_null(connection);

    will_return(__wrap_imtp_connection_get_fd, accept_fd_1);

    // mqtt_imtp_connection_accept
    connection->reader(connection->fd, imtp);
}

void test_can_read_accepted(UNUSED void** state) {
    mqtt_client_t* cl = NULL;
    amxd_object_t* client = amxd_dm_findf(mock_get_dm(), "MQTT.Client.1.");
    amxo_connection_t* connection = NULL;
    mqtt_imtp_t* imtp = NULL;

    assert_non_null(client);
    assert_non_null(client->priv);

    cl = (mqtt_client_t*) client->priv;
    assert_non_null(cl);
    imtp = (mqtt_imtp_t*) amxc_llist_it_get_data(amxc_llist_get_first(&cl->imtp_cons), mqtt_imtp_t, it);
    assert_non_null(imtp);

    connection = amxo_connection_get_first(mqtt_get_parser(), CON_ACCEPTED);
    assert_non_null(connection);

    will_return(__wrap_imtp_connection_read_frame, 0);

    // mqtt_imtp_accepted_read
    connection->reader(connection->fd, imtp);
}

void test_can_read_in_parts(UNUSED void** state) {
    mqtt_client_t* cl = NULL;
    amxd_object_t* client = amxd_dm_findf(mock_get_dm(), "MQTT.Client.1.");
    amxo_connection_t* connection = NULL;
    mqtt_imtp_t* imtp = NULL;

    assert_non_null(client);
    assert_non_null(client->priv);

    cl = (mqtt_client_t*) client->priv;
    assert_non_null(cl);
    imtp = (mqtt_imtp_t*) amxc_llist_it_get_data(amxc_llist_get_first(&cl->imtp_cons), mqtt_imtp_t, it);
    assert_non_null(imtp);

    connection = amxo_connection_get_first(mqtt_get_parser(), CON_ACCEPTED);
    assert_non_null(connection);

    will_return(__wrap_imtp_connection_read_frame, 1);

    // mqtt_imtp_accepted_read
    connection->reader(connection->fd, imtp);

    will_return(__wrap_imtp_connection_read_frame, 0);

    // mqtt_imtp_accepted_read
    connection->reader(connection->fd, imtp);
}

void test_client_can_create_second_connection(UNUSED void** state) {
    amxc_var_t args;
    amxc_var_t ret;
    amxd_object_t* object = NULL;
    mqtt_client_t* cl = NULL;
    amxo_connection_t* connection = NULL;
    amxc_llist_it_t* next = NULL;
    mqtt_imtp_t* imtp = NULL;

    amxc_var_init(&ret);
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "uri", "usp:/tmp/test2.sock");

    object = amxd_dm_findf(mock_get_dm(), "MQTT.Client.1.");
    assert_non_null(object);

    will_return(__wrap_imtp_connection_get_fd, listen_fd);
    assert_int_equal(_Client_CreateListenSocket(object, NULL, &args, &ret), amxd_status_ok);

    cl = (mqtt_client_t*) object->priv;
    assert_non_null(cl);
    next = amxc_llist_it_get_next(amxc_llist_get_first(&cl->imtp_cons));
    imtp = (mqtt_imtp_t*) amxc_llist_it_get_data(next, mqtt_imtp_t, it);
    assert_non_null(imtp);

    assert_int_equal(amxc_llist_size(&cl->imtp_cons), 2);

    // Accept second connection
    connection = amxo_connection_get_first(mqtt_get_parser(), CON_LISTEN);
    assert_non_null(connection);
    amxo_connection_get_next(mqtt_get_parser(), connection, CON_LISTEN);
    assert_non_null(connection);

    will_return(__wrap_imtp_connection_get_fd, accept_fd_2);

    // mqtt_imtp_connection_accept
    connection->reader(connection->fd, imtp);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_invalid_read_closes_connection(UNUSED void** state) {
    mqtt_client_t* cl = NULL;
    amxd_object_t* client = amxd_dm_findf(mock_get_dm(), "MQTT.Client.1.");
    amxo_connection_t* connection = NULL;
    mqtt_imtp_t* imtp = NULL;

    assert_non_null(client);
    assert_non_null(client->priv);

    cl = (mqtt_client_t*) client->priv;
    assert_non_null(cl);
    imtp = (mqtt_imtp_t*) amxc_llist_it_get_data(amxc_llist_get_first(&cl->imtp_cons), mqtt_imtp_t, it);
    assert_non_null(imtp);

    connection = amxo_connection_get_first(mqtt_get_parser(), CON_ACCEPTED);
    assert_non_null(connection);

    // Error reading
    will_return(__wrap_imtp_connection_read_frame, -1);
    // mqtt_imtp_accepted_read
    connection->reader(connection->fd, imtp);
}

void test_client_force_reconnect(UNUSED void** state) {
    amxd_object_t* object = NULL;
    amxd_status_t status = amxd_status_unknown_error;
    mqtt_client_t* client = NULL;
    amxc_var_t params;

    amxc_var_init(&params);
    object = add_enabled_client();

    expect_any(__wrap_mosquitto_disconnect_v5, mosq);
    expect_any(__wrap_mosquitto_disconnect_v5, reason_code);
    expect_any(__wrap_mosquitto_disconnect_v5, properties);
    will_return_always(__wrap_mosquitto_disconnect_v5, 0);
    expect_any(__wrap_mosquitto_destroy, mosq);

    expect_any(__wrap_mosquitto_new, id);
    expect_any(__wrap_mosquitto_new, clean_session);
    expect_any(__wrap_mosquitto_new, obj);
    will_return(__wrap_mosquitto_new, true);

    expect_any(__wrap_mosquitto_int_option, mosq);
    expect_value(__wrap_mosquitto_int_option, option, MOSQ_OPT_PROTOCOL_VERSION);
    expect_value(__wrap_mosquitto_int_option, value, MQTT_PROTOCOL_V5);

    will_return(__wrap_ares_gethostbyname, 0);
    expect_any(__wrap_mosquitto_connect_bind_async_v5, mosq);
    expect_string(__wrap_mosquitto_connect_bind_async_v5, host, "broker.hivemq.com");
    expect_value(__wrap_mosquitto_connect_bind_async_v5, port, 1883);
    expect_value(__wrap_mosquitto_connect_bind_async_v5, keepalive, 60);
    expect_any(__wrap_mosquitto_connect_bind_async_v5, bind_address);
    expect_any(__wrap_mosquitto_connect_bind_async_v5, properties);
    will_return(__wrap_mosquitto_connect_bind_async_v5, 0);

    status = _Client_ForceReconnect(object, NULL, NULL, NULL);
    assert_int_equal(status, amxd_status_deferred);

    client = (mqtt_client_t*) object->priv;
    assert_true(client->reconnect_busy);

    handle_events();
    mqtt_ctrl_handle_item(mqtt_ctrl_fd(), NULL); // mqtt_client_disconnected

    amxd_object_get_params(object, &params, amxd_dm_access_public);
    assert_string_equal(GET_CHAR(&params, "Status"), "Disabled");

    handle_events();

    amxd_object_get_params(object, &params, amxd_dm_access_public);
    assert_string_equal(GET_CHAR(&params, "Status"), "Connecting");

    mqtt_ctrl_handle_item(mqtt_ctrl_fd(), NULL); // mqtt_client_connected
    handle_events();

    amxd_object_get_params(object, &params, amxd_dm_access_public);
    amxc_var_dump(&params, STDOUT_FILENO);
    assert_string_equal(GET_CHAR(&params, "Status"), "Connected");
    assert_false(client->reconnect_busy);

    amxc_var_clean(&params);
}

void test_client_delete_during_force_reconnect(UNUSED void** state) {
    amxd_object_t* object = NULL;
    amxc_var_t params;

    amxc_var_init(&params);

    object = amxd_dm_findf(mock_get_dm(), "MQTT.Client.2.");
    assert_non_null(object);

    expect_any(__wrap_mosquitto_disconnect_v5, mosq);
    expect_any(__wrap_mosquitto_disconnect_v5, reason_code);
    expect_any(__wrap_mosquitto_disconnect_v5, properties);
    will_return_always(__wrap_mosquitto_disconnect_v5, 0);
    expect_any(__wrap_mosquitto_destroy, mosq);

    assert_int_equal(_Client_ForceReconnect(object, NULL, NULL, NULL), amxd_status_deferred);

    handle_events();
    mqtt_ctrl_handle_item(mqtt_ctrl_fd(), NULL); // mqtt_client_disconnected

    amxd_object_get_params(object, &params, amxd_dm_access_public);
    assert_string_equal(GET_CHAR(&params, "Status"), "Disabled");

    amxd_object_delete(&object);

    handle_events();

    object = amxd_dm_findf(mock_get_dm(), "MQTT.Client.2.");
    assert_null(object);

    amxc_var_clean(&params);
}

void test_client_force_reconnect_can_fail(UNUSED void** state) {
    amxd_object_t* object = NULL;
    amxd_status_t status = amxd_status_unknown_error;
    mqtt_client_t* client = NULL;
    amxc_var_t params;

    amxc_var_init(&params);
    object = add_enabled_client();

    expect_any(__wrap_mosquitto_disconnect_v5, mosq);
    expect_any(__wrap_mosquitto_disconnect_v5, reason_code);
    expect_any(__wrap_mosquitto_disconnect_v5, properties);
    will_return_always(__wrap_mosquitto_disconnect_v5, 0);
    expect_any_always(__wrap_mosquitto_destroy, mosq);

    expect_any(__wrap_mosquitto_new, id);
    expect_any(__wrap_mosquitto_new, clean_session);
    expect_any(__wrap_mosquitto_new, obj);
    will_return(__wrap_mosquitto_new, true);

    expect_any(__wrap_mosquitto_int_option, mosq);
    expect_value(__wrap_mosquitto_int_option, option, MOSQ_OPT_PROTOCOL_VERSION);
    expect_value(__wrap_mosquitto_int_option, value, MQTT_PROTOCOL_V5);

    will_return(__wrap_ares_gethostbyname, 0);
    expect_any(__wrap_mosquitto_connect_bind_async_v5, mosq);
    expect_string(__wrap_mosquitto_connect_bind_async_v5, host, "broker.hivemq.com");
    expect_value(__wrap_mosquitto_connect_bind_async_v5, port, 1883);
    expect_value(__wrap_mosquitto_connect_bind_async_v5, keepalive, 60);
    expect_any(__wrap_mosquitto_connect_bind_async_v5, bind_address);
    expect_any(__wrap_mosquitto_connect_bind_async_v5, properties);
    will_return(__wrap_mosquitto_connect_bind_async_v5, -1);

    status = _Client_ForceReconnect(object, NULL, NULL, NULL);
    assert_int_equal(status, amxd_status_deferred);

    client = (mqtt_client_t*) object->priv;
    assert_true(client->reconnect_busy);

    handle_events();
    mqtt_ctrl_handle_item(mqtt_ctrl_fd(), NULL); // mqtt_client_disconnected

    amxd_object_get_params(object, &params, amxd_dm_access_public);
    assert_string_equal(GET_CHAR(&params, "Status"), "Disabled");

    handle_events();

    amxd_object_get_params(object, &params, amxd_dm_access_public);
    assert_string_equal(GET_CHAR(&params, "Status"), "Error");

    amxd_object_get_params(object, &params, amxd_dm_access_public);
    amxc_var_dump(&params, STDOUT_FILENO);
    assert_false(client->reconnect_busy);

    amxc_var_clean(&params);
}
