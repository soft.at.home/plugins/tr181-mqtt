/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef __TEST_MOSQUITTO_MOCK_H__
#define __TEST_MOSQUITTO_MOCK_H__

#include "mock.h"

int __wrap_mosquitto_lib_init(void);
int __wrap_mosquitto_lib_cleanup(void);
struct  mosquitto* __wrap_mosquitto_new(const char* id, bool clean_session, void* obj);
void __wrap_mosquitto_destroy(struct mosquitto* mosq);
int __wrap_mosquitto_int_option(struct  mosquitto* mosq, enum mosq_opt_t option, int value);
void __wrap_mosquitto_connect_v5_callback_set(struct mosquitto* mosq, void (* on_connect)(struct mosquitto*, void*, int, int, const mosquitto_property* props));
void __wrap_mosquitto_disconnect_v5_callback_set(struct mosquitto* mosq, void (* on_disconnect)(struct mosquitto*, void*, int, const mosquitto_property*));
void __wrap_mosquitto_publish_v5_callback_set(struct mosquitto* mosq,
                                              void (* on_publish)(struct mosquitto*, void*, int, int, const mosquitto_property*));
void __wrap_mosquitto_message_v5_callback_set(struct mosquitto* mosq,
                                              void (* on_message)(struct mosquitto*, void*, const struct mosquitto_message*, const mosquitto_property*));
void __wrap_mosquitto_subscribe_v5_callback_set(struct mosquitto* mosq,
                                                void (* on_subscribe_v5)(struct mosquitto*, void* userdata, int mid, int qos_count, const int* granted_qos, const mosquitto_property* props));
void __wrap_mosquitto_unsubscribe_v5_callback_set(struct mosquitto* mosq,
                                                  void (* on_unsubscribe_v5)(struct mosquitto*, void* userdata, int mid, const mosquitto_property* props));
void __wrap_mosquitto_log_callback_set(struct mosquitto* mosq,
                                       void (* on_log)(struct mosquitto*, void*, int, const char*));
int __wrap_mosquitto_string_to_property_info(const char* propname, int* identifier, int* type);
void __wrap_mosquitto_user_data_set(struct mosquitto* mosq, void* obj);
int __wrap_mosquitto_property_add_byte(mosquitto_property** proplist, int identifier, uint8_t value);
int __wrap_mosquitto_property_add_string_pair(mosquitto_property** proplist, int identifier, const char* name, const char* value);
int __wrap_mosquitto_property_add_string(mosquitto_property** proplist, int identifier, const char* value);
int __wrap_mosquitto_loop_read(struct mosquitto* mosq, int max_packets);
int __wrap_mosquitto_socket(struct mosquitto* mosq);
int __wrap_mosquitto_connect_bind_async_v5(struct mosquitto* mosq, const char* host, int port, int keepalive, const char* bind_address, const mosquitto_property* properties);
void __wrap_mosquitto_property_free_all(mosquitto_property** properties);
int __wrap_mosquitto_disconnect_v5(struct mosquitto* mosq, int reason_code, const mosquitto_property* properties);
int __wrap_mosquitto_subscribe_v5(struct mosquitto* mosq, int* mid, const char* sub, int qos, int options, const mosquitto_property* properties);
int __wrap_mosquitto_unsubscribe_v5(struct mosquitto* mosq, int* mid, const char* sub, const mosquitto_property* properties);
int __wrap_mosquitto_username_pw_set(struct mosquitto* mosq, const char* username, const char* password);
int __wrap_mosquitto_publish_v5(struct mosquitto* mosq, int* mid, const char* topic, int payloadlen, const void* payload, int qos, bool retain, const mosquitto_property* properties);
int __wrap_mosquitto_topic_matches_sub(const char* sub, const char* topic, bool* result);
int __wrap_mosquitto_tls_set(struct mosquitto* mosq, const char* cafile, const char* capath, const char* certfile, const char* keyfile, int (* pw_callback)(char* buf, int size, int rwflag, void* userdata));
int __wrap_mosquitto_tls_insecure_set(struct mosquitto* mosq, bool value);
int __wrap_mosquitto_loop_misc(struct mosquitto* mosq);
int __wrap_mosquitto_loop_write(struct mosquitto* mosq, int max_packets);
int __wrap_mosquitto_send_connect(struct mosquitto* mosq);
int __wrap_mosquitto_string_option(struct mosquitto* mosq, enum mosq_opt_t option, const char* value);

void mock_send_message(void* wm, void* priv, char* topic, char* payload);
#endif // __TEST_MOSQUITTO_MOCK_H__
