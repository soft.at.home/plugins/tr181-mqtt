/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include "mqtt.h"
#include "dm_mqtt.h"
#include "mqtt_interface.h"

#include "mock.h"
#include "mosquitto_mock.h"
#include "test_mqtt_connect_disconnect.h"
#include "test_common.h"

static amxo_parser_t parser;
static const char* odl_defs = "../../odl/tr181-mqtt_definition.odl";

static void mqtt_connection_wait_write(UNUSED const char* const sig_name,
                                       UNUSED const amxc_var_t* const data,
                                       void* const priv) {
    printf("Called mqtt_connection_wait_write\n");
    amxd_object_t* client_object = (amxd_object_t*) priv;
    mqtt_client_t* cl = NULL;

    assert_non_null(client_object);
    assert_non_null(client_object->priv);

    cl = (mqtt_client_t*) client_object->priv;

    mqtt_finish_connect(-1, cl->con);
}

int test_mqtt_setup(UNUSED void** state) {
    amxd_object_t* root_obj = NULL;
    amxd_dm_t* dm = mock_init_dm();
    int retval = 0;

    assert_int_equal(amxo_parser_init(&parser), 0);

    root_obj = amxd_dm_get_root(dm);
    assert_non_null(root_obj);

    amxo_resolver_ftab_add(&parser, "mqtt_instance_is_valid", AMXO_FUNC(_mqtt_instance_is_valid));
    amxo_resolver_ftab_add(&parser, "mqtt_instance_cleanup", AMXO_FUNC(_mqtt_instance_cleanup));

    amxo_resolver_ftab_add(&parser, "mqtt_client_toggled", AMXO_FUNC(_mqtt_client_toggled));
    amxo_resolver_ftab_add(&parser, "mqtt_client_disabled", AMXO_FUNC(_mqtt_client_disabled));
    amxo_resolver_ftab_add(&parser, "mqtt_client_added", AMXO_FUNC(_mqtt_client_added));
    amxo_resolver_ftab_add(&parser, "mqtt_client_auto_reconnect", AMXO_FUNC(_mqtt_client_auto_reconnect));

    amxo_resolver_ftab_add(&parser, "print_event", AMXO_FUNC(_print_event));

    retval = amxo_parser_parse_file(&parser, odl_defs, root_obj);
    assert_int_equal(retval, 0);

    capture_sigalrm();
    handle_events();

    will_return(__wrap_amxb_listen, -1); // setup direct pcb listen socket
    assert_int_equal(_mqtt_main(0, dm, &parser), 0);

    return 0;
}

int test_mqtt_teardown(UNUSED void** state) {
    amxd_dm_t* dm = mock_get_dm();

    assert_int_equal(_mqtt_main(1, dm, &parser), 0);
    expect_any_count(__wrap_mosquitto_destroy, mosq, 7);

    amxo_parser_clean(&parser);
    mock_cleanup_dm();

    return 0;
}

void test_can_create_client(UNUSED void** state) {
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t* params = NULL;
    amxd_object_t* client = amxd_dm_findf(mock_get_dm(), "MQTT.Client");
    const char* name = "MQTT-Test1";

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    params = amxc_var_add_key(amxc_htable_t, &args, "parameters", NULL);
    amxc_var_add_key(cstring_t, params, "BrokerAddress", "broker.hivemq.com");
    amxc_var_add_key(cstring_t, params, "ClientID", name);
    amxc_var_add_key(cstring_t, params, "Name", name);
    assert_int_equal(amxd_object_invoke_function(client, "_add", &args, &ret), 0);

    amxc_var_dump(&ret, STDOUT_FILENO);
    client = amxd_dm_findf(mock_get_dm(), "MQTT.Client.%d", GET_UINT32(&ret, "index"));
    assert_non_null(client);
    client = amxd_dm_findf(mock_get_dm(), "MQTT.Client.%s", GET_CHAR(&ret, "name"));
    assert_non_null(client);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    assert_int_equal(amxd_object_invoke_function(client, "_get", &args, &ret), 0);
    amxc_var_dump(&ret, STDOUT_FILENO);

    handle_events();

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_can_connect_client(UNUSED void** state) {
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t* params = NULL;
    amxd_object_t* client = amxd_dm_findf(mock_get_dm(), "MQTT.Client.1.");

    amxc_var_init(&args);
    amxc_var_init(&ret);

    assert_null(client->priv);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    params = amxc_var_add_key(amxc_htable_t, &args, "parameters", NULL);
    amxc_var_add_key(bool, params, "Enable", true);
    assert_int_equal(amxd_object_invoke_function(client, "_set", &args, &ret), 0);

    expect_any(__wrap_mosquitto_new, id);
    expect_any(__wrap_mosquitto_new, clean_session);
    expect_any(__wrap_mosquitto_new, obj);
    will_return(__wrap_mosquitto_new, true);

    expect_any(__wrap_mosquitto_int_option, mosq);
    expect_value(__wrap_mosquitto_int_option, option, MOSQ_OPT_PROTOCOL_VERSION);
    expect_value(__wrap_mosquitto_int_option, value, MQTT_PROTOCOL_V5);

    will_return(__wrap_ares_gethostbyname, 0);
    expect_any(__wrap_mosquitto_connect_bind_async_v5, mosq);
    expect_string(__wrap_mosquitto_connect_bind_async_v5, host, "broker.hivemq.com");
    expect_value(__wrap_mosquitto_connect_bind_async_v5, port, 1883);
    expect_value(__wrap_mosquitto_connect_bind_async_v5, keepalive, 60);
    expect_any(__wrap_mosquitto_connect_bind_async_v5, bind_address);
    expect_any(__wrap_mosquitto_connect_bind_async_v5, properties);
    will_return(__wrap_mosquitto_connect_bind_async_v5, 0);

    handle_events();
    read_sigalrm();
    amxp_timers_calculate();
    amxp_timers_check();

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    assert_int_equal(amxd_object_invoke_function(client, "_get", &args, &ret), 0);
    amxc_var_dump(&ret, STDOUT_FILENO);

    params = amxc_var_get_key(&ret, "MQTT.Client.1.", AMXC_VAR_FLAG_DEFAULT);
    assert_string_equal(GET_CHAR(params, "Status"), "Connecting");

    mqtt_ctrl_handle_item(mqtt_ctrl_fd(), NULL);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    assert_int_equal(amxd_object_invoke_function(client, "_get", &args, &ret), 0);
    amxc_var_dump(&ret, STDOUT_FILENO);

    params = amxc_var_get_key(&ret, "MQTT.Client.1.", AMXC_VAR_FLAG_DEFAULT);
    assert_string_equal(GET_CHAR(params, "Status"), "Connected");

    assert_non_null(client->priv);
    assert_non_null(((mqtt_client_t*) (client->priv))->con);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_can_disconnect_client(UNUSED void** state) {
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t* params = NULL;
    amxd_object_t* client = amxd_dm_findf(mock_get_dm(), "MQTT.Client.1.");

    amxc_var_init(&args);
    amxc_var_init(&ret);

    assert_non_null(client->priv);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    params = amxc_var_add_key(amxc_htable_t, &args, "parameters", NULL);
    amxc_var_add_key(bool, params, "Enable", false);
    assert_int_equal(amxd_object_invoke_function(client, "_set", &args, &ret), 0);

    expect_any(__wrap_mosquitto_disconnect_v5, mosq);
    expect_any(__wrap_mosquitto_disconnect_v5, reason_code);
    expect_any(__wrap_mosquitto_disconnect_v5, properties);
    will_return_always(__wrap_mosquitto_disconnect_v5, 0);

    handle_events();

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    assert_int_equal(amxd_object_invoke_function(client, "_get", &args, &ret), 0);
    amxc_var_dump(&ret, STDOUT_FILENO);

    params = amxc_var_get_key(&ret, "MQTT.Client.1.", AMXC_VAR_FLAG_DEFAULT);
    assert_string_equal(GET_CHAR(params, "Status"), "Disabled");

    expect_any(__wrap_mosquitto_destroy, mosq);

    mqtt_ctrl_handle_item(mqtt_ctrl_fd(), NULL);

    assert_non_null(client->priv);
    assert_null(((mqtt_client_t*) (client->priv))->con);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_can_not_enable_client_with_empty_broker_address(UNUSED void** state) {
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t* params = NULL;
    amxd_object_t* client = amxd_dm_findf(mock_get_dm(), "MQTT.Client.1.");

    amxc_var_init(&args);
    amxc_var_init(&ret);

    assert_non_null(client->priv);
    assert_null(((mqtt_client_t*) (client->priv))->con);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    params = amxc_var_add_key(amxc_htable_t, &args, "parameters", NULL);
    amxc_var_add_key(cstring_t, params, "BrokerAddress", "");
    amxc_var_add_key(bool, params, "Enable", true);
    assert_int_not_equal(amxd_object_invoke_function(client, "_set", &args, &ret), 0);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    assert_int_equal(amxd_object_invoke_function(client, "_get", &args, &ret), 0);
    amxc_var_dump(&ret, STDOUT_FILENO);

    params = amxc_var_get_key(&ret, "MQTT.Client.1.", AMXC_VAR_FLAG_DEFAULT);
    assert_string_equal(GET_CHAR(params, "Status"), "Disabled");
    assert_string_equal(GET_CHAR(params, "BrokerAddress"), "broker.hivemq.com");

    assert_non_null(client->priv);
    assert_null(((mqtt_client_t*) (client->priv))->con);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_can_not_enable_mqtt3_with_empty_id_and_clean_session_false(UNUSED void** state) {
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t* params = NULL;
    amxd_object_t* client = amxd_dm_findf(mock_get_dm(), "MQTT.Client.1.");

    amxc_var_init(&args);
    amxc_var_init(&ret);

    assert_non_null(client->priv);
    assert_null(((mqtt_client_t*) (client->priv))->con);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    params = amxc_var_add_key(amxc_htable_t, &args, "parameters", NULL);
    amxc_var_add_key(bool, params, "Enable", true);
    amxc_var_add_key(cstring_t, params, "ProtocolVersion", "3.1.1");
    amxc_var_add_key(bool, params, "CleanSession", false);
    amxc_var_add_key(cstring_t, params, "ClientID", "");
    assert_int_not_equal(amxd_object_invoke_function(client, "_set", &args, &ret), 0);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    assert_int_equal(amxd_object_invoke_function(client, "_get", &args, &ret), 0);
    amxc_var_dump(&ret, STDOUT_FILENO);

    params = amxc_var_get_key(&ret, "MQTT.Client.1.", AMXC_VAR_FLAG_DEFAULT);
    assert_string_equal(GET_CHAR(params, "Status"), "Disabled");
    assert_string_equal(GET_CHAR(params, "ProtocolVersion"), "5.0");

    assert_non_null(client->priv);
    assert_null(((mqtt_client_t*) (client->priv))->con);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_can_enable_mqtt3_with_client_id_and_clean_session_false(UNUSED void** state) {
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t* params = NULL;
    amxd_object_t* client = amxd_dm_findf(mock_get_dm(), "MQTT.Client.1.");

    amxc_var_init(&args);
    amxc_var_init(&ret);

    assert_non_null(client->priv);
    assert_null(((mqtt_client_t*) (client->priv))->con);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    params = amxc_var_add_key(amxc_htable_t, &args, "parameters", NULL);
    amxc_var_add_key(bool, params, "Enable", true);
    amxc_var_add_key(cstring_t, params, "ProtocolVersion", "3.1.1");
    amxc_var_add_key(bool, params, "CleanSession", false);
    assert_int_equal(amxd_object_invoke_function(client, "_set", &args, &ret), 0);

    expect_any(__wrap_mosquitto_new, id);
    expect_any(__wrap_mosquitto_new, clean_session);
    expect_any(__wrap_mosquitto_new, obj);
    will_return(__wrap_mosquitto_new, true);

    will_return(__wrap_ares_gethostbyname, 0);
    expect_any(__wrap_mosquitto_connect_bind_async_v5, mosq);
    expect_string(__wrap_mosquitto_connect_bind_async_v5, host, "broker.hivemq.com");
    expect_value(__wrap_mosquitto_connect_bind_async_v5, port, 1883);
    expect_value(__wrap_mosquitto_connect_bind_async_v5, keepalive, 60);
    expect_any(__wrap_mosquitto_connect_bind_async_v5, bind_address);
    expect_any(__wrap_mosquitto_connect_bind_async_v5, properties);
    will_return(__wrap_mosquitto_connect_bind_async_v5, 0);

    handle_events();
    mqtt_ctrl_handle_item(mqtt_ctrl_fd(), NULL);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    assert_int_equal(amxd_object_invoke_function(client, "_get", &args, &ret), 0);
    amxc_var_dump(&ret, STDOUT_FILENO);

    params = amxc_var_get_key(&ret, "MQTT.Client.1.", AMXC_VAR_FLAG_DEFAULT);
    assert_string_equal(GET_CHAR(params, "Status"), "Connected");
    assert_string_equal(GET_CHAR(params, "ProtocolVersion"), "3.1.1");

    assert_non_null(client->priv);
    assert_non_null(((mqtt_client_t*) (client->priv))->con);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_can_delete_client_if_connected(UNUSED void** state) {
    amxc_var_t args;
    amxc_var_t ret;
    amxd_object_t* client = amxd_dm_findf(mock_get_dm(), "MQTT.Client.1.");

    amxc_var_init(&args);
    amxc_var_init(&ret);

    assert_non_null(client->priv);
    assert_non_null(((mqtt_client_t*) (client->priv))->con);

    expect_any(__wrap_mosquitto_destroy, mosq);

    client = amxd_object_get_parent(client);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(uint32_t, &args, "index", 1);
    assert_int_equal(amxd_object_invoke_function(client, "_del", &args, &ret), 0);
    amxc_var_dump(&ret, STDOUT_FILENO);

    handle_events();
    mqtt_ctrl_handle_item(mqtt_ctrl_fd(), NULL);
    handle_events();

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_can_create_enabled_client(UNUSED void** state) {
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t* params = NULL;
    amxd_object_t* client = amxd_dm_findf(mock_get_dm(), "MQTT.Client");
    const char* name = "MQTT-Test2";

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    params = amxc_var_add_key(amxc_htable_t, &args, "parameters", NULL);
    amxc_var_add_key(bool, params, "Enable", true);
    amxc_var_add_key(cstring_t, params, "BrokerAddress", "broker.hivemq.com");
    amxc_var_add_key(cstring_t, params, "ClientID", name);
    amxc_var_add_key(cstring_t, params, "ProtocolVersion", "3.1");
    amxc_var_add_key(cstring_t, params, "Name", name);
    assert_int_equal(amxd_object_invoke_function(client, "_add", &args, &ret), 0);

    amxc_var_dump(&ret, STDOUT_FILENO);
    client = amxd_dm_findf(mock_get_dm(), "MQTT.Client.%d", GET_UINT32(&ret, "index"));
    assert_non_null(client);
    client = amxd_dm_findf(mock_get_dm(), "MQTT.Client.%s", GET_CHAR(&ret, "name"));
    assert_non_null(client);

    expect_any(__wrap_mosquitto_new, id);
    expect_any(__wrap_mosquitto_new, clean_session);
    expect_any(__wrap_mosquitto_new, obj);
    will_return(__wrap_mosquitto_new, true);

    expect_any(__wrap_mosquitto_int_option, mosq);
    expect_value(__wrap_mosquitto_int_option, option, MOSQ_OPT_PROTOCOL_VERSION);
    expect_value(__wrap_mosquitto_int_option, value, MQTT_PROTOCOL_V31);

    will_return(__wrap_ares_gethostbyname, 0);
    expect_any(__wrap_mosquitto_connect_bind_async_v5, mosq);
    expect_string(__wrap_mosquitto_connect_bind_async_v5, host, "broker.hivemq.com");
    expect_value(__wrap_mosquitto_connect_bind_async_v5, port, 1883);
    expect_value(__wrap_mosquitto_connect_bind_async_v5, keepalive, 60);
    expect_any(__wrap_mosquitto_connect_bind_async_v5, bind_address);
    expect_any(__wrap_mosquitto_connect_bind_async_v5, properties);
    will_return(__wrap_mosquitto_connect_bind_async_v5, 0);

    handle_events();
    mqtt_ctrl_handle_item(mqtt_ctrl_fd(), NULL);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    assert_int_equal(amxd_object_invoke_function(client, "_get", &args, &ret), 0);
    amxc_var_dump(&ret, STDOUT_FILENO);

    params = amxc_var_get_key(&ret, "MQTT.Client.2.", AMXC_VAR_FLAG_DEFAULT);
    assert_string_equal(GET_CHAR(params, "Status"), "Connected");
    assert_string_equal(GET_CHAR(params, "ProtocolVersion"), "3.1");

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_can_handle_mosquitto_error(UNUSED void** state) {
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t* params = NULL;
    amxd_object_t* client = amxd_dm_findf(mock_get_dm(), "MQTT.Client");
    const char* name = "MQTT-Test3";

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    params = amxc_var_add_key(amxc_htable_t, &args, "parameters", NULL);
    amxc_var_add_key(bool, params, "Enable", true);
    amxc_var_add_key(cstring_t, params, "BrokerAddress", "broker.hivemq.com");
    amxc_var_add_key(cstring_t, params, "ClientID", name);
    amxc_var_add_key(cstring_t, params, "Name", name);
    assert_int_equal(amxd_object_invoke_function(client, "_add", &args, &ret), 0);

    amxc_var_dump(&ret, STDOUT_FILENO);
    client = amxd_dm_findf(mock_get_dm(), "MQTT.Client.%d", GET_UINT32(&ret, "index"));
    assert_non_null(client);
    client = amxd_dm_findf(mock_get_dm(), "MQTT.Client.%s", GET_CHAR(&ret, "name"));
    assert_non_null(client);

    expect_any(__wrap_mosquitto_new, id);
    expect_any(__wrap_mosquitto_new, clean_session);
    expect_any(__wrap_mosquitto_new, obj);
    will_return(__wrap_mosquitto_new, false);

    handle_events();
    mqtt_ctrl_handle_item(mqtt_ctrl_fd(), NULL);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    assert_int_equal(amxd_object_invoke_function(client, "_get", &args, &ret), 0);
    amxc_var_dump(&ret, STDOUT_FILENO);

    params = amxc_var_get_key(&ret, "MQTT.Client.3.", AMXC_VAR_FLAG_DEFAULT);
    assert_string_equal(GET_CHAR(params, "Status"), "Error_Misconfigured");
    assert_string_equal(GET_CHAR(params, "ProtocolVersion"), "5.0");
    assert_true(GET_BOOL(params, "Enable")); // Is this correct?

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_client_takes_mqtt5_properties(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_dm_t* dm = mock_get_dm();
    amxc_var_t params;
    amxd_object_t* client = amxd_dm_findf(dm, "MQTT.Client.3.");

    amxd_trans_init(&trans);
    amxc_var_init(&params);

    amxd_trans_select_pathf(&trans, "MQTT.Client.3.");
    amxd_trans_set_value(cstring_t, &trans, "ClientID", "");
    amxd_trans_set_value(bool, &trans, "Enable", false);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);

    handle_events();
    mqtt_ctrl_handle_item(mqtt_ctrl_fd(), NULL);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MQTT.Client.3.");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);

    expect_any(__wrap_mosquitto_new, id);
    expect_any(__wrap_mosquitto_new, clean_session);
    expect_any(__wrap_mosquitto_new, obj);
    will_return(__wrap_mosquitto_new, true);

    expect_any(__wrap_mosquitto_int_option, mosq);
    expect_value(__wrap_mosquitto_int_option, option, MOSQ_OPT_PROTOCOL_VERSION);
    expect_value(__wrap_mosquitto_int_option, value, MQTT_PROTOCOL_V5);

    will_return(__wrap_ares_gethostbyname, 0);
    expect_any(__wrap_mosquitto_connect_bind_async_v5, mosq);
    expect_string(__wrap_mosquitto_connect_bind_async_v5, host, "broker.hivemq.com");
    expect_value(__wrap_mosquitto_connect_bind_async_v5, port, 1883);
    expect_value(__wrap_mosquitto_connect_bind_async_v5, keepalive, 60);
    expect_any(__wrap_mosquitto_connect_bind_async_v5, bind_address);
    expect_any(__wrap_mosquitto_connect_bind_async_v5, properties);
    will_return(__wrap_mosquitto_connect_bind_async_v5, 1);

    expect_any_count(__wrap_mosquitto_subscribe_v5, mosq, 3);
    expect_any_count(__wrap_mosquitto_subscribe_v5, mid, 3);
    expect_string(__wrap_mosquitto_subscribe_v5, sub, "usp/server/provided/topic");
    expect_string(__wrap_mosquitto_subscribe_v5, sub, "usp/server/backup/topic");
    expect_string(__wrap_mosquitto_subscribe_v5, sub, "response/topic");
    expect_value_count(__wrap_mosquitto_subscribe_v5, qos, 0, 3);
    expect_value_count(__wrap_mosquitto_subscribe_v5, options, 0, 3);
    expect_any_count(__wrap_mosquitto_subscribe_v5, properties, 3);

    handle_events();
    mqtt_ctrl_handle_item(mqtt_ctrl_fd(), NULL);

    assert_int_equal(amxd_object_get_params(client, &params, amxd_dm_access_protected), 0);
    amxc_var_dump(&params, STDOUT_FILENO);
    assert_string_equal(GET_CHAR(&params, "ClientID"), "Some-server-side-gen-client-id");

    handle_events();

    amxc_var_clean(&params);
    amxd_trans_clean(&trans);
}

void test_client_connect_with_user_properties(UNUSED void** state) {
    amxd_trans_t trans;
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t* params = NULL;
    amxd_dm_t* dm = mock_get_dm();
    amxd_object_t* client = NULL;
    amxd_object_t* prop = NULL;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    client = amxd_dm_findf(dm, "MQTT.Client.3.UserProperty");
    assert_non_null(client);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    params = amxc_var_add_key(amxc_htable_t, &args, "parameters", NULL);
    amxc_var_add_key(bool, params, "Enable", true);
    amxc_var_add_key(cstring_t, params, "Alias", "test-user-prop-1");
    amxc_var_add_key(cstring_t, params, "Name", "test-user-prop-1");
    amxc_var_add_key(cstring_t, params, "Value", "dummy");
    amxc_var_add_key(csv_string_t, params, "PacketType", "CONNECT");
    assert_int_equal(amxd_object_invoke_function(client, "_add", &args, &ret), 0);

    amxc_var_dump(&ret, STDOUT_FILENO);
    prop = amxd_dm_findf(mock_get_dm(), "MQTT.Client.3.UserProperty.%d", GET_UINT32(&ret, "index"));
    assert_non_null(prop);
    prop = amxd_dm_findf(mock_get_dm(), "MQTT.Client.3.UserProperty.%s", GET_CHAR(&ret, "name"));
    assert_non_null(prop);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    assert_int_equal(amxd_object_invoke_function(prop, "_get", &args, &ret), 0);
    amxc_var_dump(&ret, STDOUT_FILENO);

    amxc_var_clean(&ret);
    amxc_var_clean(&args);

    // Disable and re-enable client to pass new user properties with CONNECT
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MQTT.Client.3.");
    amxd_trans_set_value(bool, &trans, "Enable", false);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);

    expect_any(__wrap_mosquitto_disconnect_v5, mosq);
    expect_any(__wrap_mosquitto_disconnect_v5, reason_code);
    expect_any(__wrap_mosquitto_disconnect_v5, properties);
    will_return_always(__wrap_mosquitto_disconnect_v5, 0);
    expect_any(__wrap_mosquitto_destroy, mosq);

    handle_events();
    mqtt_ctrl_handle_item(mqtt_ctrl_fd(), NULL);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "MQTT.Client.3.");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);

    expect_any(__wrap_mosquitto_new, id);
    expect_any(__wrap_mosquitto_new, clean_session);
    expect_any(__wrap_mosquitto_new, obj);
    will_return(__wrap_mosquitto_new, true);

    expect_any(__wrap_mosquitto_int_option, mosq);
    expect_value(__wrap_mosquitto_int_option, option, MOSQ_OPT_PROTOCOL_VERSION);
    expect_value(__wrap_mosquitto_int_option, value, MQTT_PROTOCOL_V5);

    will_return(__wrap_ares_gethostbyname, 0);
    expect_any(__wrap_mosquitto_connect_bind_async_v5, mosq);
    expect_string(__wrap_mosquitto_connect_bind_async_v5, host, "broker.hivemq.com");
    expect_value(__wrap_mosquitto_connect_bind_async_v5, port, 1883);
    expect_value(__wrap_mosquitto_connect_bind_async_v5, keepalive, 60);
    expect_any(__wrap_mosquitto_connect_bind_async_v5, bind_address);
    expect_any(__wrap_mosquitto_connect_bind_async_v5, properties);
    will_return(__wrap_mosquitto_connect_bind_async_v5, 0);

    handle_events();
    mqtt_ctrl_handle_item(mqtt_ctrl_fd(), NULL);
}

void test_client_tls_connection(UNUSED void** state) {
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t* params = NULL;
    amxd_object_t* client = amxd_dm_findf(mock_get_dm(), "MQTT.Client");
    const char* name = "MQTT-Test4";

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    params = amxc_var_add_key(amxc_htable_t, &args, "parameters", NULL);
    amxc_var_add_key(bool, params, "Enable", true);
    amxc_var_add_key(cstring_t, params, "BrokerAddress", "test.mosquitto.org");
    amxc_var_add_key(cstring_t, params, "BrokerPort", "8884");
    amxc_var_add_key(cstring_t, params, "ClientID", name);
    amxc_var_add_key(cstring_t, params, "Name", name);
    amxc_var_add_key(cstring_t, params, "TransportProtocol", "TLS");
    amxc_var_add_key(cstring_t, params, "CACertificate", "/tmp/ca.crt");
    amxc_var_add_key(cstring_t, params, "ClientCertificate", "/tmp/client.crt");
    amxc_var_add_key(cstring_t, params, "PrivateKey", "/tmp/client.key");
    amxc_var_add_key(bool, params, "CheckServerHostName", true);
    assert_int_equal(amxd_object_invoke_function(client, "_add", &args, &ret), 0);

    amxc_var_dump(&ret, STDOUT_FILENO);
    client = amxd_dm_findf(mock_get_dm(), "MQTT.Client.%d", GET_UINT32(&ret, "index"));
    assert_non_null(client);
    client = amxd_dm_findf(mock_get_dm(), "MQTT.Client.%s", GET_CHAR(&ret, "name"));
    assert_non_null(client);

    expect_any(__wrap_mosquitto_new, id);
    expect_any(__wrap_mosquitto_new, clean_session);
    expect_any(__wrap_mosquitto_new, obj);
    will_return(__wrap_mosquitto_new, true);

    expect_any(__wrap_mosquitto_int_option, mosq);
    expect_value(__wrap_mosquitto_int_option, option, MOSQ_OPT_PROTOCOL_VERSION);
    expect_value(__wrap_mosquitto_int_option, value, MQTT_PROTOCOL_V5);

    expect_any(__wrap_mosquitto_tls_insecure_set, mosq);
    expect_value(__wrap_mosquitto_tls_insecure_set, value, false);
    will_return(__wrap_mosquitto_tls_insecure_set, 0);

    expect_any(__wrap_mosquitto_tls_set, mosq);
    expect_string(__wrap_mosquitto_tls_set, cafile, "/tmp/ca.crt");
    expect_any(__wrap_mosquitto_tls_set, capath);
    expect_string(__wrap_mosquitto_tls_set, certfile, "/tmp/client.crt");
    expect_string(__wrap_mosquitto_tls_set, keyfile, "/tmp/client.key");
    expect_any(__wrap_mosquitto_tls_set, pw_callback);
    will_return(__wrap_mosquitto_tls_set, 0);

    will_return(__wrap_ares_gethostbyname, 0);
    expect_any(__wrap_mosquitto_connect_bind_async_v5, mosq);
    expect_string(__wrap_mosquitto_connect_bind_async_v5, host, "test.mosquitto.org");
    expect_value(__wrap_mosquitto_connect_bind_async_v5, port, 8884);
    expect_value(__wrap_mosquitto_connect_bind_async_v5, keepalive, 60);
    expect_any(__wrap_mosquitto_connect_bind_async_v5, bind_address);
    expect_any(__wrap_mosquitto_connect_bind_async_v5, properties);
    will_return(__wrap_mosquitto_connect_bind_async_v5, 0);

    handle_events();
    mqtt_ctrl_handle_item(mqtt_ctrl_fd(), NULL);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    assert_int_equal(amxd_object_invoke_function(client, "_get", &args, &ret), 0);
    amxc_var_dump(&ret, STDOUT_FILENO);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_client_tls_connection_insecure(UNUSED void** state) {
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t* params = NULL;
    amxd_object_t* client = amxd_dm_findf(mock_get_dm(), "MQTT.Client");
    const char* name = "MQTT-Test5";

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    params = amxc_var_add_key(amxc_htable_t, &args, "parameters", NULL);
    amxc_var_add_key(bool, params, "Enable", true);
    amxc_var_add_key(cstring_t, params, "BrokerAddress", "test.mosquitto.org");
    amxc_var_add_key(cstring_t, params, "BrokerPort", "8884");
    amxc_var_add_key(cstring_t, params, "ClientID", name);
    amxc_var_add_key(cstring_t, params, "Name", name);
    amxc_var_add_key(cstring_t, params, "TransportProtocol", "TLS");
    amxc_var_add_key(cstring_t, params, "CACertificate", "/tmp/ca.crt");
    amxc_var_add_key(cstring_t, params, "ClientCertificate", "/tmp/client.crt");
    amxc_var_add_key(cstring_t, params, "PrivateKey", "/tmp/client.key");
    amxc_var_add_key(bool, params, "CheckServerHostName", false);
    assert_int_equal(amxd_object_invoke_function(client, "_add", &args, &ret), 0);

    amxc_var_dump(&ret, STDOUT_FILENO);
    client = amxd_dm_findf(mock_get_dm(), "MQTT.Client.%d", GET_UINT32(&ret, "index"));
    assert_non_null(client);
    client = amxd_dm_findf(mock_get_dm(), "MQTT.Client.%s", GET_CHAR(&ret, "name"));
    assert_non_null(client);

    expect_any(__wrap_mosquitto_new, id);
    expect_any(__wrap_mosquitto_new, clean_session);
    expect_any(__wrap_mosquitto_new, obj);
    will_return(__wrap_mosquitto_new, true);

    expect_any(__wrap_mosquitto_int_option, mosq);
    expect_value(__wrap_mosquitto_int_option, option, MOSQ_OPT_PROTOCOL_VERSION);
    expect_value(__wrap_mosquitto_int_option, value, MQTT_PROTOCOL_V5);

    expect_any(__wrap_mosquitto_tls_insecure_set, mosq);
    expect_value(__wrap_mosquitto_tls_insecure_set, value, true);
    will_return(__wrap_mosquitto_tls_insecure_set, 0);

    expect_any(__wrap_mosquitto_tls_set, mosq);
    expect_string(__wrap_mosquitto_tls_set, cafile, "/tmp/ca.crt");
    expect_any(__wrap_mosquitto_tls_set, capath);
    expect_string(__wrap_mosquitto_tls_set, certfile, "/tmp/client.crt");
    expect_string(__wrap_mosquitto_tls_set, keyfile, "/tmp/client.key");
    expect_any(__wrap_mosquitto_tls_set, pw_callback);
    will_return(__wrap_mosquitto_tls_set, 0);

    will_return(__wrap_ares_gethostbyname, 0);
    expect_any(__wrap_mosquitto_connect_bind_async_v5, mosq);
    expect_string(__wrap_mosquitto_connect_bind_async_v5, host, "test.mosquitto.org");
    expect_value(__wrap_mosquitto_connect_bind_async_v5, port, 8884);
    expect_value(__wrap_mosquitto_connect_bind_async_v5, keepalive, 60);
    expect_any(__wrap_mosquitto_connect_bind_async_v5, bind_address);
    expect_any(__wrap_mosquitto_connect_bind_async_v5, properties);
    will_return(__wrap_mosquitto_connect_bind_async_v5, 0);

    handle_events();
    mqtt_ctrl_handle_item(mqtt_ctrl_fd(), NULL);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    assert_int_equal(amxd_object_invoke_function(client, "_get", &args, &ret), 0);
    amxc_var_dump(&ret, STDOUT_FILENO);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_client_tls_connection_invalid(UNUSED void** state) {
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t* params = NULL;
    amxd_object_t* client = amxd_dm_findf(mock_get_dm(), "MQTT.Client");
    amxc_string_t client_path;
    const char* name = "MQTT-Test6";

    amxc_string_init(&client_path, 0);
    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    params = amxc_var_add_key(amxc_htable_t, &args, "parameters", NULL);
    amxc_var_add_key(bool, params, "Enable", true);
    amxc_var_add_key(cstring_t, params, "BrokerAddress", "test.mosquitto.org");
    amxc_var_add_key(cstring_t, params, "BrokerPort", "8884");
    amxc_var_add_key(cstring_t, params, "ClientID", name);
    amxc_var_add_key(cstring_t, params, "Name", name);
    amxc_var_add_key(cstring_t, params, "TransportProtocol", "TLS");
    amxc_var_add_key(cstring_t, params, "CACertificate", "/tmp/ca.crt");
    amxc_var_add_key(cstring_t, params, "ClientCertificate", "/tmp/client.crt");
    amxc_var_add_key(cstring_t, params, "PrivateKey", "/tmp/client.key");
    amxc_var_add_key(bool, params, "CheckServerHostName", true);
    assert_int_equal(amxd_object_invoke_function(client, "_add", &args, &ret), 0);

    amxc_var_dump(&ret, STDOUT_FILENO);
    amxc_string_setf(&client_path, "MQTT.Client.%d.", GET_UINT32(&ret, "index"));
    client = amxd_dm_findf(mock_get_dm(), "%s", amxc_string_get(&client_path, 0));
    assert_non_null(client);
    client = amxd_dm_findf(mock_get_dm(), "MQTT.Client.%s", GET_CHAR(&ret, "name"));
    assert_non_null(client);

    expect_any(__wrap_mosquitto_new, id);
    expect_any(__wrap_mosquitto_new, clean_session);
    expect_any(__wrap_mosquitto_new, obj);
    will_return(__wrap_mosquitto_new, true);

    expect_any(__wrap_mosquitto_int_option, mosq);
    expect_value(__wrap_mosquitto_int_option, option, MOSQ_OPT_PROTOCOL_VERSION);
    expect_value(__wrap_mosquitto_int_option, value, MQTT_PROTOCOL_V5);

    expect_any(__wrap_mosquitto_tls_insecure_set, mosq);
    expect_value(__wrap_mosquitto_tls_insecure_set, value, false);
    will_return(__wrap_mosquitto_tls_insecure_set, 0);

    expect_any(__wrap_mosquitto_tls_set, mosq);
    expect_string(__wrap_mosquitto_tls_set, cafile, "/tmp/ca.crt");
    expect_any(__wrap_mosquitto_tls_set, capath);
    expect_string(__wrap_mosquitto_tls_set, certfile, "/tmp/client.crt");
    expect_string(__wrap_mosquitto_tls_set, keyfile, "/tmp/client.key");
    expect_any(__wrap_mosquitto_tls_set, pw_callback);
    will_return(__wrap_mosquitto_tls_set, -1);

    handle_events();
    mqtt_ctrl_handle_item(mqtt_ctrl_fd(), NULL);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    assert_int_equal(amxd_object_invoke_function(client, "_get", &args, &ret), 0);
    amxc_var_dump(&ret, STDOUT_FILENO);

    params = GET_ARG(&ret, amxc_string_get(&client_path, 0));
    assert_string_equal(GET_CHAR(params, "Status"), "Error_Misconfigured");
    assert_string_equal(GET_CHAR(params, "ProtocolVersion"), "5.0");
    assert_true(GET_BOOL(params, "Enable")); // Is this correct?

    amxc_string_clean(&client_path);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_client_connect_async(UNUSED void** state) {
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t* params = NULL;
    amxd_object_t* client = amxd_dm_findf(mock_get_dm(), "MQTT.Client");
    amxc_string_t client_path;
    const char* name = "MQTT-Test-async";

    amxp_sigmngr_add_signal(NULL, "connection-wait-write");

    amxc_string_init(&client_path, 0);
    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    params = amxc_var_add_key(amxc_htable_t, &args, "parameters", NULL);
    amxc_var_add_key(bool, params, "Enable", true);
    amxc_var_add_key(cstring_t, params, "BrokerAddress", "broker.hivemq.com");
    amxc_var_add_key(cstring_t, params, "ClientID", name);
    amxc_var_add_key(cstring_t, params, "Name", name);
    amxc_var_add_key(cstring_t, params, "ProtocolVersion", "5.0");
    assert_int_equal(amxd_object_invoke_function(client, "_add", &args, &ret), 0);

    amxc_var_dump(&ret, STDOUT_FILENO);
    amxc_string_setf(&client_path, "MQTT.Client.%d.", GET_UINT32(&ret, "index"));
    client = amxd_dm_findf(mock_get_dm(), "%s", amxc_string_get(&client_path, 0));
    assert_non_null(client);
    assert_null(client->priv); // events not yet handled

    amxp_slot_connect(NULL, "connection-wait-write", NULL, mqtt_connection_wait_write, client);

    expect_any(__wrap_mosquitto_new, id);
    expect_any(__wrap_mosquitto_new, clean_session);
    expect_any(__wrap_mosquitto_new, obj);
    will_return(__wrap_mosquitto_new, true);

    expect_any(__wrap_mosquitto_int_option, mosq);
    expect_value(__wrap_mosquitto_int_option, option, MOSQ_OPT_PROTOCOL_VERSION);
    expect_value(__wrap_mosquitto_int_option, value, MQTT_PROTOCOL_V5);

    will_return(__wrap_ares_gethostbyname, 0);
    expect_any(__wrap_mosquitto_connect_bind_async_v5, mosq);
    expect_string(__wrap_mosquitto_connect_bind_async_v5, host, "broker.hivemq.com");
    expect_value(__wrap_mosquitto_connect_bind_async_v5, port, 1883);
    expect_value(__wrap_mosquitto_connect_bind_async_v5, keepalive, 60);
    expect_any(__wrap_mosquitto_connect_bind_async_v5, bind_address);
    expect_any(__wrap_mosquitto_connect_bind_async_v5, properties);
    will_return(__wrap_mosquitto_connect_bind_async_v5, MOSQ_ERR_CONN_INPROGRESS);

    handle_events();

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    assert_int_equal(amxd_object_invoke_function(client, "_get", &args, &ret), 0);
    amxc_var_dump(&ret, STDOUT_FILENO);
    params = GET_ARG(&ret, amxc_string_get(&client_path, 0));
    assert_string_equal(GET_CHAR(params, "Status"), "Connecting");

    mqtt_ctrl_handle_item(mqtt_ctrl_fd(), NULL); // handle on_connect

    assert_int_equal(amxd_object_invoke_function(client, "_get", &args, &ret), 0);
    amxc_var_dump(&ret, STDOUT_FILENO);
    params = GET_ARG(&ret, amxc_string_get(&client_path, 0));
    assert_string_equal(GET_CHAR(params, "Status"), "Connected");

    amxc_string_clean(&client_path);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_client_auto_reconnect_on_change(UNUSED void** state) {
    amxc_var_t args;
    amxc_var_t ret;
    amxd_trans_t trans;
    amxc_var_t* params = NULL;
    amxd_object_t* client = amxd_dm_findf(mock_get_dm(), "MQTT.Client");
    amxc_string_t client_path;
    const char* new_broker = "test.mosquitto.org";
    const char* name = "MQTT-Test-auto-reconnect";

    amxc_string_init(&client_path, 0);
    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxd_trans_init(&trans);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    params = amxc_var_add_key(amxc_htable_t, &args, "parameters", NULL);
    amxc_var_add_key(bool, params, "Enable", true);
    amxc_var_add_key(cstring_t, params, "BrokerAddress", "broker.hivemq.com");
    amxc_var_add_key(cstring_t, params, "ClientID", name);
    amxc_var_add_key(cstring_t, params, "Name", name);
    amxc_var_add_key(cstring_t, params, "ProtocolVersion", "5.0");
    amxc_var_add_key(bool, params, "AutoReconnect", true);
    assert_int_equal(amxd_object_invoke_function(client, "_add", &args, &ret), 0);

    amxc_var_dump(&ret, STDOUT_FILENO);
    amxc_string_setf(&client_path, "MQTT.Client.%d.", GET_UINT32(&ret, "index"));
    client = amxd_dm_findf(mock_get_dm(), "%s", amxc_string_get(&client_path, 0));
    assert_non_null(client);
    assert_null(client->priv); // events not yet handled

    expect_any(__wrap_mosquitto_new, id);
    expect_any(__wrap_mosquitto_new, clean_session);
    expect_any(__wrap_mosquitto_new, obj);
    will_return(__wrap_mosquitto_new, true);

    expect_any(__wrap_mosquitto_int_option, mosq);
    expect_value(__wrap_mosquitto_int_option, option, MOSQ_OPT_PROTOCOL_VERSION);
    expect_value(__wrap_mosquitto_int_option, value, MQTT_PROTOCOL_V5);

    will_return(__wrap_ares_gethostbyname, 0);
    expect_any(__wrap_mosquitto_connect_bind_async_v5, mosq);
    expect_string(__wrap_mosquitto_connect_bind_async_v5, host, "broker.hivemq.com");
    expect_value(__wrap_mosquitto_connect_bind_async_v5, port, 1883);
    expect_value(__wrap_mosquitto_connect_bind_async_v5, keepalive, 60);
    expect_any(__wrap_mosquitto_connect_bind_async_v5, bind_address);
    expect_any(__wrap_mosquitto_connect_bind_async_v5, properties);
    will_return(__wrap_mosquitto_connect_bind_async_v5, 0);

    handle_events();

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    assert_int_equal(amxd_object_invoke_function(client, "_get", &args, &ret), 0);
    amxc_var_dump(&ret, STDOUT_FILENO);
    params = GET_ARG(&ret, amxc_string_get(&client_path, 0));
    assert_string_equal(GET_CHAR(params, "Status"), "Connecting");

    mqtt_ctrl_handle_item(mqtt_ctrl_fd(), NULL); // handle on_connect

    assert_int_equal(amxd_object_invoke_function(client, "_get", &args, &ret), 0);
    amxc_var_dump(&ret, STDOUT_FILENO);
    params = GET_ARG(&ret, amxc_string_get(&client_path, 0));
    assert_string_equal(GET_CHAR(params, "Status"), "Connected");

    // Change the BrokerAddress and reconnect
    amxd_trans_select_pathf(&trans, "%s", amxc_string_get(&client_path, 0));
    amxd_trans_set_value(cstring_t, &trans, "BrokerAddress", new_broker);
    assert_int_equal(amxd_trans_apply(&trans, mock_get_dm()), 0);

    expect_any(__wrap_mosquitto_disconnect_v5, mosq);
    expect_any(__wrap_mosquitto_disconnect_v5, reason_code);
    expect_any(__wrap_mosquitto_disconnect_v5, properties);
    will_return_always(__wrap_mosquitto_disconnect_v5, 0);
    expect_any(__wrap_mosquitto_destroy, mosq);

    expect_any(__wrap_mosquitto_new, id);
    expect_any(__wrap_mosquitto_new, clean_session);
    expect_any(__wrap_mosquitto_new, obj);
    will_return(__wrap_mosquitto_new, true);

    expect_any(__wrap_mosquitto_int_option, mosq);
    expect_value(__wrap_mosquitto_int_option, option, MOSQ_OPT_PROTOCOL_VERSION);
    expect_value(__wrap_mosquitto_int_option, value, MQTT_PROTOCOL_V5);

    will_return(__wrap_ares_gethostbyname, 0);
    expect_any(__wrap_mosquitto_connect_bind_async_v5, mosq);
    expect_string(__wrap_mosquitto_connect_bind_async_v5, host, new_broker);
    expect_value(__wrap_mosquitto_connect_bind_async_v5, port, 1883);
    expect_value(__wrap_mosquitto_connect_bind_async_v5, keepalive, 60);
    expect_any(__wrap_mosquitto_connect_bind_async_v5, bind_address);
    expect_any(__wrap_mosquitto_connect_bind_async_v5, properties);
    will_return(__wrap_mosquitto_connect_bind_async_v5, 0);

    handle_events();
    mqtt_ctrl_handle_item(mqtt_ctrl_fd(), NULL); // handle on_disconnect

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    assert_int_equal(amxd_object_invoke_function(client, "_get", &args, &ret), 0);
    amxc_var_dump(&ret, STDOUT_FILENO);
    params = GET_ARG(&ret, amxc_string_get(&client_path, 0));
    assert_string_equal(GET_CHAR(params, "Status"), "Disabled");

    handle_events();
    read_sigalrm();
    amxp_timers_calculate();
    amxp_timers_check();
    mqtt_ctrl_handle_item(mqtt_ctrl_fd(), NULL); // handle on_connect

    assert_int_equal(amxd_object_invoke_function(client, "_get", &args, &ret), 0);
    amxc_var_dump(&ret, STDOUT_FILENO);
    params = GET_ARG(&ret, amxc_string_get(&client_path, 0));
    assert_string_equal(GET_CHAR(params, "Status"), "Connected");

    amxc_string_clean(&client_path);
    amxd_trans_clean(&trans);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}
