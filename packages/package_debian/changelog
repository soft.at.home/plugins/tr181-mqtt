tr181-mqtt (3.2.1) unstable; urgency=medium

  * Refactor libamxo - libamxp: move fd and connection management out of libamxo

 -- SAH bot <sahbot@softathome.com>  Thu, 09 Nov 2023 09:28:46 +0000

tr181-mqtt (3.2.0) unstable; urgency=medium

  * [MQTT] Remove dependency on USP backend

 -- SAH bot <sahbot@softathome.com>  Tue, 07 Nov 2023 10:51:24 +0000

tr181-mqtt (3.1.4) unstable; urgency=medium

  *  All applications using sahtrace logs should use default log levels

 -- SAH bot <sahbot@softathome.com>  Fri, 13 Oct 2023 14:14:04 +0000

tr181-mqtt (3.1.3) unstable; urgency=medium

  * Reenable persistency for tr181-mqtt

 -- SAH bot <sahbot@softathome.com>  Fri, 29 Sep 2023 14:09:00 +0000

tr181-mqtt (3.1.2) unstable; urgency=medium

  * [tr181-pcm] All the plugins that are register to PCM should set the pcm_svc_config

 -- SAH bot <sahbot@softathome.com>  Tue, 19 Sep 2023 15:10:57 +0000

tr181-mqtt (3.1.1) unstable; urgency=medium

  * [USP] The board fails to connect to the MQTT broker in TCP/IP

 -- SAH bot <sahbot@softathome.com>  Fri, 18 Aug 2023 10:22:16 +0000

tr181-mqtt (3.1.0) unstable; urgency=medium

  * Add SSL Engine support to TR181-MQTT

 -- SAH bot <sahbot@softathome.com>  Thu, 17 Aug 2023 10:23:47 +0000

tr181-mqtt (3.0.2) unstable; urgency=medium

  * lib amx mqtt client only receives messages from tr181-mqtt with topic set to NULL

 -- SAH bot <sahbot@softathome.com>  Wed, 16 Aug 2023 14:39:45 +0000

tr181-mqtt (3.0.1) unstable; urgency=medium

  * [USP] Activation is failing during onboarding state - Onboarding is not working after a Factory Reset

 -- SAH bot <sahbot@softathome.com>  Thu, 03 Aug 2023 08:55:22 +0000

tr181-mqtt (3.0.0) unstable; urgency=medium

  * [IMTP] Implement IMTP communication as specified in TR-369

 -- SAH bot <sahbot@softathome.com>  Tue, 11 Jul 2023 11:27:43 +0000

tr181-mqtt (2.12.13) unstable; urgency=medium

  * [REGRESSION][USP][CDROUTER] usp-endpoint-id missing in the MQTT CONNECT packet

 -- SAH bot <sahbot@softathome.com>  Thu, 06 Jul 2023 08:06:42 +0000

tr181-mqtt (2.12.12) unstable; urgency=medium

  * Init script has no shutdown function

 -- SAH bot <sahbot@softathome.com>  Mon, 03 Jul 2023 13:27:05 +0000

tr181-mqtt (2.12.11) unstable; urgency=medium

  * [CR9HF] - Multiple tr181-mqtt sessions running

 -- SAH bot <sahbot@softathome.com>  Tue, 27 Jun 2023 07:50:14 +0000

tr181-mqtt (2.12.10) unstable; urgency=medium

  * [V12][MQTT]: Very low frequency of Retry Mechanism

 -- SAH bot <sahbot@softathome.com>  Fri, 23 Jun 2023 08:27:22 +0000

tr181-mqtt (2.12.9) unstable; urgency=medium

  * USP MQTT Client cannot connect to USP Broker

 -- SAH bot <sahbot@softathome.com>  Thu, 15 Jun 2023 08:39:20 +0000

tr181-mqtt (2.12.8) unstable; urgency=medium

  * [USP][MQTT] Missing unique keys for MQTT data model

 -- SAH bot <sahbot@softathome.com>  Fri, 26 May 2023 07:24:02 +0000

tr181-mqtt (2.12.7) unstable; urgency=medium

  * [MQTT] Client Status no longer goes to Error

 -- SAH bot <sahbot@softathome.com>  Mon, 08 May 2023 07:15:25 +0000

tr181-mqtt (2.12.6) unstable; urgency=medium

  * [USP][DNS] Resolution is not triggered systematically when Wan was NOK and recover

 -- SAH bot <sahbot@softathome.com>  Fri, 28 Apr 2023 11:45:15 +0000

tr181-mqtt (2.12.5) unstable; urgency=medium

  * [V12][23.04.12-D] RG onboarding behaves differently on VDSL lines and on FIBER (possible DNS resolution timing issue on VDSL?)

 -- SAH bot <sahbot@softathome.com>  Fri, 21 Apr 2023 10:32:31 +0000

tr181-mqtt (2.12.4) unstable; urgency=medium

  * Make sure BrokerAddress is used in connect

 -- SAH bot <sahbot@softathome.com>  Thu, 13 Apr 2023 12:03:51 +0000

tr181-mqtt (2.12.3) unstable; urgency=medium

  * Must use hostname in connect for certificate checking

 -- SAH bot <sahbot@softathome.com>  Wed, 05 Apr 2023 14:16:02 +0000

tr181-mqtt (2.12.2) unstable; urgency=medium

  * [MQTT][USP] Make client auto reconnect configurable

 -- SAH bot <sahbot@softathome.com>  Wed, 05 Apr 2023 10:43:51 +0000

tr181-mqtt (2.12.1) unstable; urgency=medium

  * [ACS] [V12]: No Request sent from Device side and Inconsistent onboarding behaviour of V12 on MQTT server

 -- SAH bot <sahbot@softathome.com>  Mon, 03 Apr 2023 08:43:31 +0000

tr181-mqtt (2.12.0) unstable; urgency=medium

  * Use sah_lib_c-ares instead of opensource_c-ares

 -- SAH bot <sahbot@softathome.com>  Thu, 23 Mar 2023 15:23:05 +0000

tr181-mqtt (2.11.1) unstable; urgency=medium

  * [MQTT] DNS resolving can still block

 -- SAH bot <sahbot@softathome.com>  Mon, 20 Mar 2023 11:28:58 +0000

tr181-mqtt (2.11.0) unstable; urgency=medium

  * Exposure of Device.MQTT.Client. instances for both Uber/BDD/Lansec and USP

 -- SAH bot <sahbot@softathome.com>  Wed, 15 Mar 2023 11:29:22 +0000

tr181-mqtt (2.10.4) unstable; urgency=medium

  * [Config] enable configurable coredump generation

 -- SAH bot <sahbot@softathome.com>  Thu, 09 Mar 2023 09:19:15 +0000

tr181-mqtt (2.10.3) unstable; urgency=medium

  * [V12] [USP] GET Message Object = Device. - Retry mechanism is not working

 -- SAH bot <sahbot@softathome.com>  Tue, 28 Feb 2023 07:11:31 +0000

tr181-mqtt (2.10.2) unstable; urgency=medium

  * [MQTT] Remove protected attribute from Interface parameter

 -- SAH bot <sahbot@softathome.com>  Fri, 24 Feb 2023 11:42:13 +0000

tr181-mqtt (2.10.1) unstable; urgency=medium

  * [USP] Redirection to Management Broker without the need of ForceReconnect() command

 -- SAH bot <sahbot@softathome.com>  Thu, 23 Feb 2023 09:25:11 +0000

tr181-mqtt (2.10.0) unstable; urgency=medium

  * [PRPL] Make MSS work on prpl config

 -- SAH bot <sahbot@softathome.com>  Tue, 21 Feb 2023 15:45:01 +0000

tr181-mqtt (2.9.5) unstable; urgency=medium

  * Add tr181-mqtt/tr181-localagent/uspagent into processmonitor
  * Setup direct pcb connection for tr181-localagent

 -- SAH bot <sahbot@softathome.com>  Thu, 16 Feb 2023 10:29:46 +0000

tr181-mqtt (2.9.4) unstable; urgency=medium

  * [SAHPairing] Make sahpairing work with MQTT client

 -- SAH bot <sahbot@softathome.com>  Tue, 07 Feb 2023 08:36:16 +0000

tr181-mqtt (2.9.3) unstable; urgency=medium

  * [MQTT][USP] Overlapping reconnects can cause a segmentation fault

 -- SAH bot <sahbot@softathome.com>  Tue, 31 Jan 2023 08:15:13 +0000

tr181-mqtt (2.9.2) unstable; urgency=medium

  * [KPN SW2][Security]Restrict ACL of admin user

 -- SAH bot <sahbot@softathome.com>  Fri, 06 Jan 2023 13:04:22 +0000

tr181-mqtt (2.9.1) unstable; urgency=medium

  * [TR181-MQTT Client] tr181-mqtt wants a non empty ClientID with MQTT 3.1.1

 -- SAH bot <sahbot@softathome.com>  Mon, 02 Jan 2023 07:39:56 +0000

tr181-mqtt (2.9.0) unstable; urgency=medium

  * Add interop with different MQTT Broker version

 -- SAH bot <sahbot@softathome.com>  Tue, 13 Dec 2022 09:04:47 +0000

tr181-mqtt (2.8.1) unstable; urgency=medium

  * [Config] coredump generation should be configurable

 -- SAH bot <sahbot@softathome.com>  Fri, 09 Dec 2022 09:29:22 +0000

tr181-mqtt (2.8.0) unstable; urgency=medium

  * Add TcpSendMem parameter to Client object

 -- SAH bot <sahbot@softathome.com>  Wed, 16 Nov 2022 08:04:04 +0000

tr181-mqtt (2.7.3) unstable; urgency=medium

  * It must be possible to accept multiple MQTT connections per Client

 -- SAH bot <sahbot@softathome.com>  Wed, 26 Oct 2022 06:54:37 +0000

tr181-mqtt (2.7.2) unstable; urgency=medium

  * [MQTT] Segmentation fault in tr181-mqtt after enabling client

 -- SAH bot <sahbot@softathome.com>  Mon, 24 Oct 2022 10:00:12 +0000

tr181-mqtt (2.7.1) unstable; urgency=medium

  * Disable opensource CI
  * Use sah-lib-mosquitto-dev in debian package dependencies

 -- SAH bot <sahbot@softathome.com>  Tue, 20 Sep 2022 11:52:38 +0000

tr181-mqtt (2.7.0) unstable; urgency=medium

  * [BART] Create ambiorix bart module

 -- SAH bot <sahbot@softathome.com>  Thu, 15 Sep 2022 12:53:27 +0000

tr181-mqtt (2.6.0) unstable; urgency=medium

  * [USP] Open source tr181-mqtt
  * [BART] Create ambiorix bart module

 -- SAH bot <sahbot@softathome.com>  Fri, 09 Sep 2022 15:25:57 +0000

tr181-mqtt (2.5.3) unstable; urgency=medium

  * Certificate and config to connect to FUT backend

 -- SAH bot <sahbot@softathome.com>  Wed, 20 Jul 2022 08:39:07 +0000

tr181-mqtt (2.5.2) unstable; urgency=medium

  * [KPN][ROB] CPU increased has been observed

 -- SAH bot <sahbot@softathome.com>  Mon, 18 Jul 2022 07:39:19 +0000

tr181-mqtt (2.5.1) unstable; urgency=medium

  * [MQTT] TLS parameters should be persistent

 -- SAH bot <sahbot@softathome.com>  Tue, 05 Jul 2022 07:28:08 +0000

tr181-mqtt (2.5.0) unstable; urgency=medium

  * Support TLS for MQTT.Client instance used for USP

 -- SAH bot <sahbot@softathome.com>  Mon, 20 Jun 2022 15:08:36 +0000

tr181-mqtt (2.4.7) unstable; urgency=medium

  * [amx][MQTT][Client] The MQTT client must support a (tr181) retransmission scheme

 -- SAH bot <sahbot@softathome.com>  Thu, 09 Jun 2022 08:10:42 +0000

tr181-mqtt (2.4.6) unstable; urgency=medium

  * [Gitlab CI][Unit tests][valgrind] Pipeline doesn't stop when memory leaks are detected

 -- SAH bot <sahbot@softathome.com>  Thu, 19 May 2022 12:47:01 +0000

tr181-mqtt (2.4.5) unstable; urgency=medium

  * [MQTT] Topic must be writable after creation

 -- SAH bot <sahbot@softathome.com>  Thu, 05 May 2022 12:55:55 +0000

tr181-mqtt (2.4.4) unstable; urgency=medium

  * [MQTT] Clients connecting with the same ID cause conflicts

 -- SAH bot <sahbot@softathome.com>  Fri, 29 Apr 2022 14:40:34 +0000

tr181-mqtt (2.4.3) unstable; urgency=medium

  * [AMX] Hidden parameter cannot be saved correctly

 -- SAH bot <sahbot@softathome.com>  Thu, 21 Apr 2022 08:04:17 +0000

tr181-mqtt (2.4.2) unstable; urgency=medium

  * amxp timer works with miliseconds

 -- SAH bot <sahbot@softathome.com>  Fri, 15 Apr 2022 06:36:15 +0000

tr181-mqtt (2.4.1) unstable; urgency=medium

  * [Buildsystem] Pipes are stripped from odl files when they are installed

 -- Johan JACOBS <johan.jacobs_ext@softathome.com>  Wed, 13 Apr 2022 09:47:33 +0000

tr181-mqtt (2.4.0) unstable; urgency=medium

  * [amx][MQTT][Client] The MQTT client must support a (tr181) retransmission scheme

 -- SAH bot <sahbot@softathome.com>  Tue, 12 Apr 2022 16:16:16 +0000

tr181-mqtt (2.3.9) unstable; urgency=medium

  * [USP] Re-enable data model persistency in uspagent and tr181-mqtt

 -- SAH bot <sahbot@softathome.com>  Mon, 28 Mar 2022 09:11:08 +0000

tr181-mqtt (2.3.8) unstable; urgency=medium

  * [MQTT] Legacy code needs to be removed

 -- SAH bot <sahbot@softathome.com>  Thu, 24 Mar 2022 14:20:56 +0000

tr181-mqtt (2.3.7) unstable; urgency=medium

  * [GetDebugInformation] Add data model debuginfo in component services

 -- SAH bot <sahbot@softathome.com>  Thu, 24 Mar 2022 10:45:53 +0000

tr181-mqtt (2.3.6) unstable; urgency=medium

  * Set KeepAliveTime to 0 by default [revert]

 -- SAH bot <sahbot@softathome.com>  Mon, 21 Mar 2022 12:52:41 +0000

tr181-mqtt (2.3.5) unstable; urgency=medium

  * Doc generation for tr181-mqtt component (amx)

 -- SAH bot <sahbot@softathome.com>  Fri, 18 Mar 2022 10:05:31 +0000

tr181-mqtt (2.3.4) unstable; urgency=medium

  * Set KeepAliveTime to 0 by default

 -- SAH bot <sahbot@softathome.com>  Mon, 14 Mar 2022 14:20:54 +0000

tr181-mqtt (2.3.3) unstable; urgency=medium

  * Enable core dumps by default
  * [USPAgent] The USP Agent must be opensourced to gitlab.com/soft.at.home

 -- SAH bot <sahbot@softathome.com>  Fri, 25 Feb 2022 11:47:19 +0000

tr181-mqtt (2.3.2) unstable; urgency=medium

  * [IMTP] Need to reread large IMTP messages

 -- SAH bot <sahbot@softathome.com>  Fri, 28 Jan 2022 14:20:40 +0000

tr181-mqtt (2.3.1) unstable; urgency=medium

  * [MQTT] Properties should only be added for MQTT 5.0

 -- SAH bot <sahbot@softathome.com>  Thu, 06 Jan 2022 08:31:10 +0000

tr181-mqtt (2.3.0) unstable; urgency=medium

  * tr181-mqtt has trouble connecting due to DNS issue at boot

 -- SAH bot <sahbot@softathome.com>  Tue, 04 Jan 2022 16:35:10 +0000

tr181-mqtt (2.2.6) unstable; urgency=medium

  * [MQTT] tr181-mqtt should not try to extract msg id from TLV

 -- SAH bot <sahbot@softathome.com>  Fri, 17 Dec 2021 13:13:39 +0000

tr181-mqtt (2.2.5) unstable; urgency=medium

  * [USP] Agent must publish responses on controller's reply-to topic

 -- SAH bot <sahbot@softathome.com>  Mon, 13 Dec 2021 11:58:03 +0000

tr181-mqtt (2.2.4) unstable; urgency=medium

  * Segmentation fault when disconnecting client

 -- SAH bot <sahbot@softathome.com>  Mon, 11 Oct 2021 14:51:24 +0000

tr181-mqtt (2.2.3) unstable; urgency=medium

  * [ACL] Add default ACL configuration per services
  * [USP] MQTT clients should send ping messages to stay connected
  * Add example ACL files

 -- SAH bot <sahbot@softathome.com>  Fri, 08 Oct 2021 11:59:49 +0000

tr181-mqtt (2.2.2) unstable; urgency=medium

  * Issue #27: Remove references to lib_mosquitto_poc from README
  * [USP] The USP Agent and MQTT client should have the USP backend as run time dependency

 -- SAH bot <sahbot@softathome.com>  Thu, 16 Sep 2021 07:19:05 +0000

tr181-mqtt (2.2.1) unstable; urgency=medium

  * [AMX] Debian packages for ambiorix plugins should create symlinks to amxrt

 -- SAH bot <sahbot@softathome.com>  Thu, 09 Sep 2021 12:05:39 +0000

tr181-mqtt (2.2.0) unstable; urgency=medium

  * Extend logging for MQTT properties

 -- SAH bot <sahbot@softathome.com>  Tue, 31 Aug 2021 06:09:53 +0000

tr181-mqtt (2.1.3) unstable; urgency=medium

  * tr181-mqtt should use tagged version of libmosquitto

 -- SAH bot <sahbot@softathome.com>  Fri, 27 Aug 2021 06:33:34 +0000

tr181-mqtt (2.1.2) unstable; urgency=medium

  * Remove socket fd from event loop before disconnecting the client

 -- SAH bot <sahbot@softathome.com>  Wed, 25 Aug 2021 13:19:45 +0000

tr181-mqtt (2.1.1) unstable; urgency=medium

  * USP components should have debian packages

 -- SAH bot <sahbot@softathome.com>  Tue, 24 Aug 2021 12:03:18 +0000

tr181-mqtt (2.1.0) unstable; urgency=medium

  * [USP][CDROUTER] USP Agent should provide a reply-to topic in MQTT messages

 -- Johan Jacobs <johan.jacobs_ext@softathome.com>  Mon, 23 Aug 2021 15:12:40 +0000
