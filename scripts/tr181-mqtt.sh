#!/bin/sh
[ -f /etc/environment ] && source /etc/environment
ulimit -c ${ULIMIT_CONFIGURATION:-0}
name="tr181-mqtt"

case $1 in
    start|boot)
        source /etc/environment
        if [ -s /var/run/tr181-mqtt.pid ] && [ -d "/proc/$(cat /var/run/tr181-mqtt.pid)/fdinfo" ]; then
           echo "tr181-mqtt is already started"
           exit 0
        fi
        tr181-mqtt -D
        ;;
    stop|shutdown)
        if [ -f /var/run/${name}.pid ]; then
            kill `cat /var/run/${name}.pid`
        else
            killall ${name}
        fi
        ;;
    debuginfo)
        ubus-cli "MQTT.?"
        ;;
    restart)
        $0 stop
        $0 start
        ;;
    fail)
        /etc/init.d/uspagent stop
        /etc/init.d/tr181-localagent stop
        /etc/init.d/tr181-mqtt start
        /etc/init.d/tr181-localagent start
        /etc/init.d/uspagent start
        ;;
    log)
        echo "TODO log tr181 mqtt client"
        ;;
    *)
        echo "Usage : $0 [start|boot|stop|debuginfo|log]"
        ;;
esac
