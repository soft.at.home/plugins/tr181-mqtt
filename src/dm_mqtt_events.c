/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <imtp/imtp_message.h>
#include <imtp/imtp_connection.h>

#include <debug/sahtrace.h>

#include "mqtt.h"
#include "dm_mqtt.h"
#include "dm_mqtt_utilities.h"
#include "mqtt_interface.h"
#include "mqtt_imtp.h"

static int mqtt_subscription_exists(mqtt_con_t* con, char* topic) {
    int retval = -1;
    mqtt_sub_t* sub_ref = NULL;
    const char* topic_found = NULL;
    amxc_var_t params;
    amxc_var_init(&params);

    if(topic == NULL) {
        goto exit;
    }

    amxc_llist_for_each(it, &con->subs) {
        sub_ref = amxc_llist_it_get_data(it, mqtt_sub_t, it);
        if(sub_ref->topic != NULL) {
            if(strcmp(sub_ref->topic, topic) == 0) {
                SAH_TRACEZ_INFO(ME, "Subscription for topic [%s] exists", topic);
                goto exit;
            }
        } else if(sub_ref->sub_obj != NULL) {
            amxd_object_get_params(sub_ref->sub_obj, &params, amxd_dm_access_protected);
            topic_found = GET_CHAR(&params, "Topic");
            if(strcmp(topic_found, topic) == 0) {
                SAH_TRACEZ_INFO(ME, "Subscription for topic [%s] exists", topic);
                goto exit;
            }
        }
    }

    retval = 0;
exit:
    amxc_var_clean(&params);
    return retval;
}

static void mqtt_sub_create(mqtt_con_t* con, char* topic) {
    int retval = -1;
    int32_t msg_id = 0;
    mqtt_sub_t* sub_ref = NULL;

    retval = mqtt_subscription_exists(con, topic);
    when_failed(retval, exit);

    sub_ref = (mqtt_sub_t*) calloc(1, sizeof(mqtt_sub_t));
    when_null(sub_ref, exit);

    amxc_llist_append(&con->subs, &sub_ref->it);

    retval = mqtt_subscribe(con, &msg_id, topic, 0, 0);

    sub_ref->topic = topic;
    sub_ref->message_id = msg_id;
    sub_ref->status = mqtt_sub_status_subscribing;
exit:
    if(retval < 0) {
        free(sub_ref);
        sub_ref = NULL;
    } else {
        if(sub_ref != NULL) {
            sub_ref->status = mqtt_sub_status_subscribing;
        }
    }
}

/* Create mqtt subscriptions based on user properties in the CONNACK */
static void mqtt_user_prop_subscriptions(mqtt_con_t* con,
                                         amxc_var_t* props) {
    amxc_var_t* topics = NULL;
    char* topic = NULL;
    uint32_t type_id = 0;

    topics = amxc_var_get_pathf(props, AMXC_VAR_FLAG_DEFAULT, "user-property.subscribe-topic");
    type_id = amxc_var_type_of(topics);
    if(type_id == AMXC_VAR_ID_LIST) {
        amxc_var_for_each(prop, topics) {
            topic = amxc_var_dyncast(cstring_t, prop);
            SAH_TRACEZ_INFO(ME, "Creating user-propery subscription for topic %s\n", topic);
            mqtt_sub_create(con, topic);
        }
    } else if(type_id == AMXC_VAR_ID_CSTRING) {
        mqtt_sub_create(con, topic);
    }
}

/* Create mqtt subscription based on a Response Information property in the CONNACK
   and fill in MQTT.Client.{i}.ResponseInformation
 */
static void mqtt_response_topic_handle(mqtt_con_t* con,
                                       amxc_var_t* props) {
    int retval = -1;
    char* topic = NULL;
    amxd_trans_t trans;
    amxd_object_t* object = (amxd_object_t*) con->client_inst;

    when_null(object, exit);

    topic = amxc_var_dyncast(cstring_t, GET_ARG(props, "response-information"));
    if(topic != NULL) {
        SAH_TRACEZ_INFO(ME, "Got Response Information property in CONNACK: %s", topic);
        retval = mqtt_subscription_exists(con, topic);
        when_failed(retval, exit);

        mqtt_sub_create(con, topic);

        amxd_trans_init(&trans);
        amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
        amxd_trans_select_object(&trans, object);
        amxd_trans_set_value(cstring_t, &trans, "ResponseInformation", topic);
        amxd_trans_apply(&trans, mqtt_get_dm());

        amxd_trans_clean(&trans);
    }

exit:
    return;
}

static void mqtt_client_force_reconnect_done(mqtt_client_t* client) {
    SAH_TRACEZ_INFO(ME, "ForceReconnect is done successfully");
    amxd_function_deferred_done(client->reconnect_id, amxd_status_ok, NULL, NULL);
    client->reconnect_busy = false;
}

/* This function is called by the mqtt ctrl pipe. It is called after the mosquitto mqtt5_on_connect
 * callback function is triggered i.e. after receiving a CONNACK.
 */
static void mqtt_client_connected(mqtt_con_t* con,
                                  mqtt_item_t* item) {
    amxd_object_t* object = (amxd_object_t*) con->client_inst;
    amxd_trans_t trans;
    amxc_ts_t now;
    const char* acid = NULL;
    amxc_var_t* properties = amxc_var_get_key(&item->data,
                                              "properties",
                                              AMXC_VAR_FLAG_DEFAULT);
    mqtt_client_t* client = NULL;

    when_null(object, exit);
    when_null(object->priv, exit);
    SAH_TRACEZ_INFO(ME, "enter client connected");

    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);

    amxd_trans_select_object(&trans, object);
    amxd_trans_set_value(cstring_t, &trans, "Status", "Connected");

    acid = GET_CHAR(properties, "assigned-client-identifier");
    if(acid != NULL) {
        amxd_trans_set_value(cstring_t, &trans, "ClientID", acid);
    }
    client = (mqtt_client_t*) object->priv;
    // Client connected: retry counter reset on 0 and delete retry timer if any
    client->nbr_con_retries = 0;
    if(client->con_retry_timer) {
        amxp_timer_delete(&client->con_retry_timer);
    }
    client->con_retry_timer = NULL;
    // If client is connected as result of ForceReconnect, return result
    if(client->reconnect_busy) {
        mqtt_client_force_reconnect_done(client);
    }

    // Add subscriptions from user properties
    mqtt_user_prop_subscriptions(con, properties);
    mqtt_response_topic_handle(con, properties);

    amxc_ts_now(&now);
    amxd_trans_select_pathf(&trans, ".Stats");
    amxd_trans_set_value(amxc_ts_t, &trans, "BrokerConnectionEstablished", &now);
    mqtt_client_for_all(object,
                        "Subscription.[Enable == true && Status != 'Subscribed'].",
                        mqtt_client_subscribe,
                        &trans);

    amxd_trans_apply(&trans, mqtt_get_dm());
exit:
    amxd_trans_clean(&trans);
    return;
}

static void mqtt_client_disconnected(mqtt_con_t* con,
                                     UNUSED mqtt_item_t* item) {
    amxd_object_t* object = (amxd_object_t*) con->client_inst;
    mqtt_client_t* client = NULL;
    bool clean_session = false;
    amxc_var_t params;
    amxd_trans_t trans;

    amxd_trans_init(&trans);
    amxc_var_init(&params);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);

    if(object != NULL) {
        client = (mqtt_client_t*) object->priv;
    }

    if((client != NULL) && (client->con != NULL) && (client->con == con)) {
        amxd_trans_select_object(&trans, object);
        amxd_trans_set_value(cstring_t, &trans, "Status", "Disabled");

        amxd_object_get_params(object, &params, amxd_dm_access_protected);
        if(strcmp(GET_CHAR(&params, "ProtocolVersion"), "5.0") == 0) {
            clean_session = GET_BOOL(&params, "CleanStart");
        } else {
            clean_session = GET_BOOL(&params, "CleanSession");
        }
        if(clean_session) {
            client->con = NULL;
            mqtt_delete(&con);
        }
        mqtt_client_for_all(object,
                            "Subscription.[Status == 'Subscribed'].",
                            mqtt_client_sub_reset,
                            &trans);
        amxd_trans_apply(&trans, mqtt_get_dm());
    }

    amxp_sigmngr_emit_signal(NULL, "mqtt:disconnected", NULL);

    amxd_trans_clean(&trans);
    amxc_var_clean(&params);
    return;
}

static void send_amx_event(amxd_object_t* obj, mqtt_item_t* item) {
    amxc_var_t params;
    const char* server = NULL;
    const char* topic = NULL;
    char* payload_str = NULL;

    if(item == NULL) {
        SAH_TRACEZ_ERROR(ME, "Error: invalid item");
        return;
    }

    amxc_var_init(&params);
    amxc_var_set_type(&params, AMXC_VAR_ID_HTABLE);

    server = amxd_object_get_name(obj, AMXD_OBJECT_NAMED);
    amxc_var_add_key(cstring_t, &params, "Server", server);

    topic = GET_CHAR(&item->data, "topic");
    amxc_var_add_key(cstring_t, &params, "Topic", topic);

    payload_str = (char*) calloc(1, item->payloadlen + 1);
    memcpy(payload_str, item->payload, item->payloadlen);
    amxc_var_add_key(cstring_t, &params, "Payload", payload_str);
    free(payload_str);

    SAH_TRACEZ_INFO(ME, "sending event [%s] [%s]", server, topic);
    amxd_object_emit_signal(obj, "mqtt", &params);
    amxc_var_clean(&params);
}

static void mqtt_client_message(mqtt_con_t* con,
                                mqtt_item_t* item) {
    amxd_object_t* object = (amxd_object_t*) con->client_inst;
    mqtt_client_t* client = NULL;

    when_null(object, exit);
    when_null(object->priv, exit);
    client = (mqtt_client_t*) object->priv;

    SAH_TRACEZ_INFO(ME, "Message received for client - %s",
                    amxd_object_get_name(object, AMXD_OBJECT_NAMED));

    send_amx_event(object, item);

    amxc_llist_iterate(it, &client->imtp_cons) {
        mqtt_imtp_t* imtp = amxc_llist_it_get_data(it, mqtt_imtp_t, it);
        mqtt_imtp_forward_message(object, imtp, item);
    }

    client->stats.publish_recv++;

exit:
    return;
}

static amxd_object_t* mqtt_client_find_subscription(amxd_object_t* object,
                                                    int32_t msg_id) {
    amxd_object_t* subscriptions = amxd_object_get(object, "Subscription");
    amxd_object_t* sub = NULL;
    mqtt_sub_t* sub_ref = NULL;

    amxd_object_for_each(instance, it, subscriptions) {
        sub = amxc_llist_it_get_data(it, amxd_object_t, it);
        if(sub->priv == NULL) {
            sub = NULL;
            continue;
        }
        sub_ref = (mqtt_sub_t*) sub->priv;
        if(msg_id != sub_ref->message_id) {
            sub = NULL;
            continue;
        }
        break;
    }

    return sub;
}

/* Update Device.MQTT.Client.{i}.Subscription.{i}.Status to Subscribed upon SUBACK */
static void mqtt_client_subscribed(mqtt_con_t* con,
                                   mqtt_item_t* item) {
    amxd_object_t* object = (amxd_object_t*) con->client_inst;
    int32_t id = GET_UINT32(&item->data, ITEMF_MSG_ID);
    amxd_object_t* sub = mqtt_client_find_subscription(object, id);
    mqtt_client_t* client = NULL;
    mqtt_sub_t* sub_ref = NULL;

    when_null(object, exit);
    when_null(object->priv, exit);
    when_null(sub, exit);
    client = (mqtt_client_t*) object->priv;
    sub_ref = (mqtt_sub_t*) sub->priv;
    sub_ref->status = mqtt_sub_status_subscribed;
    mqtt_subscription_update_status(sub, "Subscribed");

    client->stats.subscribe_sent++;

exit:
    return;
}

static void mqtt_client_unsubscribed(mqtt_con_t* con,
                                     mqtt_item_t* item) {
    amxd_object_t* object = (amxd_object_t*) con->client_inst;
    int32_t id = GET_UINT32(&item->data, ITEMF_MSG_ID);
    amxd_object_t* sub = mqtt_client_find_subscription(object, id);
    mqtt_client_t* client = NULL;
    mqtt_sub_t* sub_ref = NULL;

    when_null(object, exit);
    when_null(object->priv, exit);
    when_null(sub, exit);

    client = (mqtt_client_t*) object->priv;
    sub_ref = (mqtt_sub_t*) sub->priv;
    sub_ref->status = mqtt_sub_status_unsubscribed;
    mqtt_subscription_update_status(sub, "Unsubscribed");

    client->stats.unsubscribe_sent++;

exit:
    return;
}

// What should happen after a message was published (upon receive of a PUBACK)
static void mqtt_client_published(UNUSED mqtt_con_t* con,
                                  UNUSED mqtt_item_t* item) {
    SAH_TRACEZ_INFO(ME, "Message published");
//exit:
    return;
}


void mqtt_set_mqtt_event_handlers(void) {
    mqtt_handler_t handlers[mqtt_item_max] = {
        NULL,
        mqtt_client_connected,      // mqtt_item_connect
        mqtt_client_disconnected,   // mqtt_item_disconnect
        mqtt_client_message,        // mqtt_item_message
        mqtt_client_published,      // mqtt_item_publish
        mqtt_client_subscribed,     // mqtt_item_subscribe
        mqtt_client_unsubscribed,   // mqtt_item_unsubscribe
    };

    for(int i = 0; i < mqtt_item_max; i++) {
        mqtt_ctrl_set_handler((mqtt_item_type_t) i, handlers[i]);
    }
}
