/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "mqtt.h"
#include "dm_mqtt.h"
#include "mqtt_interface.h"

typedef struct _stat_refs {
    const char* name;
    uint32_t type;
    int32_t offset;
} stat_ref_t;

static stat_ref_t refs[] = {
    {"PublishSent", AMXC_VAR_ID_UINT64, offsetof(mqtt_stats_t, publish_sent) },
    {"PublishReceived", AMXC_VAR_ID_UINT64, offsetof(mqtt_stats_t, publish_recv)},
    {"SubscribeSent", AMXC_VAR_ID_UINT64, offsetof(mqtt_stats_t, subscribe_sent)},
    {"UnSubscribeSent", AMXC_VAR_ID_UINT64, offsetof(mqtt_stats_t, unsubscribe_sent)},
    {"MQTTMessagesSent", AMXC_VAR_ID_UINT64, offsetof(mqtt_stats_t, mqtt_msg_sent)},
    {"MQTTMessagesReceived", AMXC_VAR_ID_UINT64, offsetof(mqtt_stats_t, mqtt_msg_recv)},
    {"ConnectionErrors", AMXC_VAR_ID_UINT32, offsetof(mqtt_stats_t, con_errors)},
    {"PublishErrors", AMXC_VAR_ID_UINT32, offsetof(mqtt_stats_t, pub_errors)},
    { NULL, 0, 0 }
};

#define STATS_VAL(type, addr, offset) \
    ((addr == NULL) ? 0 : *((type*) (((char*) addr) + offset)))

static void mqtt_client_describe_stats(amxc_var_t* ht_params,
                                       const char* name,
                                       uint32_t type_id) {
    amxc_var_t* stat_param = amxc_var_add_key(amxc_htable_t,
                                              ht_params,
                                              name,
                                              NULL);

    amxd_param_build_description(stat_param, name, type_id,
                                 SET_BIT(amxd_pattr_read_only) | SET_BIT(amxd_pattr_variable),
                                 NULL);
}

static amxc_var_t* mqtt_client_add(amxc_var_t* ht_params,
                                   mqtt_stats_t* stats,
                                   stat_ref_t* ref) {
    amxc_var_t* param = NULL;
    amxc_var_t* value = NULL;

    mqtt_client_describe_stats(ht_params, ref->name, ref->type);

    param = GET_ARG(ht_params, ref->name);
    value = GET_ARG(param, "value");
    if(ref->type == AMXC_VAR_ID_UINT64) {
        amxc_var_set(uint64_t, value, STATS_VAL(uint64_t, stats, ref->offset));
    } else {
        amxc_var_set(uint32_t, value, STATS_VAL(uint32_t, stats, ref->offset));
    }

    return param;
}

static amxd_status_t mqtt_client_filterd_stats_value(amxc_var_t* params,
                                                     const char* filter,
                                                     mqtt_stats_t* stats) {
    amxd_status_t status = amxd_status_unknown_error;
    amxp_expr_t expr;
    amxc_var_t description;
    amxp_expr_status_t expr_status = amxp_expr_status_ok;

    amxc_var_init(&description);
    amxc_var_set_type(&description, AMXC_VAR_ID_HTABLE);

    if(filter != NULL) {
        when_failed(amxp_expr_init(&expr, filter), exit);
    }

    for(int i = 0; refs[i].name != NULL; i++) {
        if(filter != NULL) {
            amxc_var_t* param = mqtt_client_add(&description, stats, &refs[i]);
            if(!amxp_expr_eval_var(&expr, param, &expr_status)) {
                amxc_var_delete(&param);
                continue;
            }
            amxc_var_delete(&param);
        }
        if(refs[i].type == AMXC_VAR_ID_UINT64) {
            amxc_var_add_key(uint64_t,
                             params,
                             refs[i].name,
                             STATS_VAL(uint64_t, stats, refs[i].offset));
        } else {
            amxc_var_add_key(uint32_t,
                             params,
                             refs[i].name,
                             STATS_VAL(uint32_t, stats, refs[i].offset));
        }
    }

    if(filter != NULL) {
        amxp_expr_clean(&expr);
    }

    status = amxd_status_ok;

exit:
    amxc_var_clean(&description);
    return status;
}

amxd_status_t _stats_read(amxd_object_t* object,
                          amxd_param_t* param,
                          amxd_action_t reason,
                          const amxc_var_t* const args,
                          amxc_var_t* const retval,
                          void* priv) {
    amxd_status_t status = amxd_action_object_read(object,
                                                   param,
                                                   reason,
                                                   args,
                                                   retval,
                                                   priv);
    mqtt_client_t* client = NULL;
    mqtt_stats_t* stats = NULL;
    amxc_string_t filter;

    amxc_string_init(&filter, 0);

    if((status != amxd_status_ok) &&
       ( status != amxd_status_parameter_not_found)) {
        goto exit;
    }

    client = (mqtt_client_t*) amxd_object_get_parent(object)->priv;
    stats = client == NULL ? NULL : &client->stats;

    status = amxd_action_object_read_filter(&filter, args);
    when_failed(status, exit);
    status = mqtt_client_filterd_stats_value(retval, amxc_string_get(&filter, 0), stats);

exit:
    amxc_string_clean(&filter);
    return status;
}

amxd_status_t _stats_list(amxd_object_t* object,
                          amxd_param_t* param,
                          amxd_action_t reason,
                          const amxc_var_t* const args,
                          amxc_var_t* const retval,
                          void* priv) {
    amxd_status_t status = amxd_action_object_list(object,
                                                   param,
                                                   reason,
                                                   args,
                                                   retval,
                                                   priv);
    amxc_var_t* params = NULL;
    if(status != amxd_status_ok) {
        goto exit;
    }
    params = amxc_var_get_path(retval, "parameters", AMXC_VAR_FLAG_DEFAULT);
    for(int i = 0; refs[i].name != NULL; i++) {
        amxc_var_add(cstring_t, params, refs[i].name);
    }

exit:
    return status;
}

amxd_status_t _stats_describe(amxd_object_t* object,
                              amxd_param_t* param,
                              amxd_action_t reason,
                              const amxc_var_t* const args,
                              amxc_var_t* const retval,
                              void* priv) {
    amxd_status_t status = amxd_action_object_describe(object,
                                                       param,
                                                       reason,
                                                       args,
                                                       retval,
                                                       priv);
    mqtt_client_t* client = NULL;
    mqtt_stats_t* stats;
    amxc_var_t* params = NULL;
    if(status != amxd_status_ok) {
        goto exit;
    }

    client = (mqtt_client_t*) amxd_object_get_parent(object)->priv;
    stats = client == NULL ? NULL : &client->stats;

    params = amxc_var_get_path(retval, "parameters", AMXC_VAR_FLAG_DEFAULT);
    for(int i = 0; refs[i].name != NULL; i++) {
        mqtt_client_add(params, stats, &refs[i]);
    }

exit:
    return status;
}
