/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#define _POSIX_C_SOURCE 200809L // needed for strdup

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include <debug/sahtrace.h>

#include "mqtt.h"
#include "dm_mqtt.h"
#include "dm_mqtt_utilities.h"
#include "mqtt_interface.h"
#include "mqtt_client_connect.h"

static void mqtt_client_update(amxd_object_t* object);

void mqtt_client_interface_query_isup_cb(UNUSED const char* sig_name,
                                         const amxc_var_t* data,
                                         void* priv) {
    amxd_object_t* object = NULL;
    mqtt_client_t* client = NULL;
    bool isup = false;

    when_null(data, exit);
    when_null(priv, exit);

    object = (amxd_object_t*) priv;
    client = (mqtt_client_t*) object->priv;
    when_null(client, exit);

    isup = amxc_var_dyncast(bool, data);
    if(isup) {
        SAH_TRACEZ_INFO(ME, "Interface %s of mqtt client is up, connect", client->interface.name);
        client->interface.isup = true;
        mqtt_client_connect(object);
    } else {
        char* status = amxd_object_get_value(cstring_t, object, "Status", NULL);
        SAH_TRACEZ_INFO(ME, "Interface %s of mqtt client is down, disconnect", client->interface.name);
        client->interface.isup = false;
        if((strcmp(status, "Connecting") == 0) ||
           (strcmp(status, "Connected") == 0)) {
            mqtt_client_disconnect(object);
        }
        free(status);
    }
exit:
    return;
}

static int mqtt_client_interface_open_queries(amxd_object_t* object) {
    int retval = -1;
    mqtt_client_t* client = NULL;

    when_null(object, exit);
    client = (mqtt_client_t*) object->priv;
    when_null(client, exit);
    /* Currently we only manage (ipv4-up, ipv6-up) interface is up, as we are not using ip socket binding at mosquitto level and do not control the src ip used by the tcp connection. */
    /* When interface is comming up we perform a mqtt connect trial and start a client connect */
    /* When down, we trigger a disconnect of the client */
    /* When we get a transition interface down-up after the client is disconnected ,
       or the reason the mqtt client was is connected was the interface down, a reconnect
       trial will happen immediately after the interface is up again */
    /* No need to manage the number of instances of the netmodel queries at application level,
       lib_netmodel takes care of it */
    if(!client->interface.query_isup) {
        SAH_TRACEZ_INFO(ME, "Open queries for client interface %s", client->interface.name);
        client->interface.query_isup = netmodel_openQuery_isUp(client->interface.name,
                                                               ME,
                                                               "ipv4-up || ( ipv6-up && global)",
                                                               "down",
                                                               mqtt_client_interface_query_isup_cb,
                                                               (void*) object);
        when_null(client->interface.query_isup, exit);
    }
    retval = 0;
exit:
    return retval;
}

static void mqtt_client_interface_close_queries(amxd_object_t* object) {
    mqtt_client_t* client = NULL;

    when_null(object, exit);
    client = (mqtt_client_t*) object->priv;
    when_null(client, exit);
    SAH_TRACEZ_INFO(ME, "Close queries for client interface %s", client->interface.name);
    netmodel_closeQuery(client->interface.query_isup);
    client->interface.query_isup = NULL;
exit:
    return;
}

static int mqtt_client_interface_init(amxd_object_t* object, const char* name) {
    int retval = -1;
    mqtt_client_t* client = NULL;

    when_null(object, exit);
    when_str_empty(name, exit);
    client = (mqtt_client_t*) object->priv;
    when_null(client, exit);
    memset(&client->interface, 0, sizeof(mqtt_client_interface_t));
    client->interface.name = strdup(name);
    retval = mqtt_client_interface_open_queries(object);
    when_failed(retval, exit);
    retval = 0;
exit:
    return retval;
}

void mqtt_client_interface_clean(amxd_object_t* object) {
    mqtt_client_t* client = NULL;

    when_null(object, exit);
    client = (mqtt_client_t*) object->priv;
    when_null(client, exit);
    if(client->interface.name) {
        mqtt_client_interface_close_queries(object);
        free(client->interface.name);
        client->interface.name = NULL;
    }
    client->interface.isup = false;
exit:
    return;
}

static int mqtt_client_new(amxd_object_t* object,
                           amxc_var_t* params,
                           mqtt_client_t** client) {
    int retval = -1;

    when_null(object, exit);
    when_null(params, exit);
    when_null(client, exit);

    if(object->priv == NULL) {
        *client = calloc(1, sizeof(mqtt_client_t));
        amxc_llist_init(&(*client)->imtp_cons);
        object->priv = *client;
    } else {
        *client = (mqtt_client_t*) object->priv;
    }

    retval = 0;
exit:
    return retval;
}

/* This function is called for each client that is enabled in the datamodel. The passed object
 * is an MQTT.Client.{i} object. The object's private data will point to an mqtt_client_t struct
 * which is allocated in this function.
 */

static void mqtt_client_update(amxd_object_t* object) {
    amxc_var_t params;
    mqtt_client_t* client = NULL;
    char* status = NULL;
    const char* interface_name = NULL;

    amxc_var_init(&params);
    when_null(object, exit);

    status = amxd_object_get_value(cstring_t, object, "Status", NULL);

    if((strcmp(status, "Connecting") == 0) ||
       (strcmp(status, "Connected") == 0)) {
        goto exit;
    }

    if(mqtt_client_new(object, &params, &client) != 0) {
        SAH_TRACEZ_ERROR(ME, "Error creating new mqtt client");
        goto exit;
    }

    amxd_object_get_params(object, &params, amxd_dm_access_protected);
    interface_name = GET_CHAR(&params, "Interface");
    mqtt_client_interface_clean(object);
    mqtt_client_interface_init(object, interface_name);
exit:
    free(status);
    amxc_var_clean(&params);
}

static int mqtt_client_start(UNUSED amxd_object_t* clients,
                             amxd_object_t* client,
                             UNUSED void* priv) {
    mqtt_client_update(client);
    mqtt_client_connect(client);
    return 0;
}

static void mqtt_client_auto_reconnect_enable(const char* client_path, amxd_object_t* object) {
    amxd_dm_t* dm = mqtt_get_dm();
    amxc_string_t expr;
    SAH_TRACEZ_INFO(ME, "Enable auto reconnect for %s.", client_path);

    amxc_string_init(&expr, 0);
    amxc_string_setf(&expr, "path == '%s.' && \
                             (contains('parameters.BrokerAddress') || \
                             contains('parameters.BrokerPort') || \
                             contains('parameters.TransportProtocol') || \
                             contains('parameters.ProtocolVersion') || \
                             contains('parameters.WillMessageExpiryInterval') || \
                             contains('parameters.Username') || \
                             contains('parameters.Password')) && \
                             !(contains('parameters.Enable'))",
                     client_path);

    amxp_slot_connect_filtered(&dm->sigmngr, "dm:object-changed", amxc_string_get(&expr, 0),
                               _mqtt_client_auto_reconnect, object);

    amxc_string_clean(&expr);
}

static void mqtt_client_auto_reconnect_disable(amxd_object_t* object) {
    amxd_dm_t* dm = mqtt_get_dm();
    SAH_TRACEZ_INFO(ME, "Disable auto reconnect for MQTT.Client.%s.", amxd_object_get_name(object, AMXD_OBJECT_INDEXED));
    amxp_slot_disconnect_with_priv(&dm->sigmngr, _mqtt_client_auto_reconnect, object);
}

static int mqtt_client_auto_reconnect_configure(UNUSED amxd_object_t* clients,
                                                amxd_object_t* client,
                                                UNUSED void* priv) {
    amxo_parser_t* parser = mqtt_get_parser();
    const char* prefix = NULL;
    char* obj_path = amxd_object_get_path(client, AMXD_OBJECT_INDEXED);
    amxc_string_t param;

    amxc_string_init(&param, 0);

    when_null(parser, exit);
    prefix = GET_CHAR(&parser->config, "prefix_");
    amxc_string_setf(&param, "%sAutoReconnect", prefix == NULL ? "" : prefix);

    if(amxd_object_get_value(bool, client, amxc_string_get(&param, 0), NULL)) {
        mqtt_client_auto_reconnect_enable(obj_path, client);
    } else {
        mqtt_client_auto_reconnect_disable(client);
    }
exit:
    free(obj_path);
    amxc_string_clean(&param);
    return 0;
}

void _mqtt_start(UNUSED const char* const sig_name,
                 UNUSED const amxc_var_t* const data,
                 UNUSED void* const priv) {
    amxd_object_t* clients = amxd_dm_findf(mqtt_get_dm(), "MQTT.Client.");

    SAH_TRACEZ_INFO(ME, "Start Clients");
    amxd_object_for_all(clients, "[Enable == true].", mqtt_client_start, NULL);
    amxd_object_for_all(clients, "*", mqtt_client_auto_reconnect_configure, NULL);
}

void _mqtt_client_transport_protocol_toggle(UNUSED const char* const sig_name,
                                            const amxc_var_t* const data,
                                            UNUSED void* const priv) {
    amxd_dm_t* dm = mqtt_get_dm();
    amxd_object_t* object = amxd_dm_signal_get_object(dm, data);
    when_null(object, exit);
    mqtt_client_t* client = (mqtt_client_t*) object->priv;
    when_null(client, exit);
    client->needs_clean_start = true;
exit:
    return;
}

void _mqtt_client_toggled(UNUSED const char* const sig_name,
                          const amxc_var_t* const data,
                          UNUSED void* const priv) {
    amxd_dm_t* dm = mqtt_get_dm();
    amxd_object_t* object = amxd_dm_signal_get_object(dm, data);
    bool enabled = GETP_BOOL(data, "parameters.Enable.to");
    mqtt_client_reconnect_reset(object);
    if(enabled) {
        mqtt_client_update(object);
        mqtt_client_connect(object);
    } else {
        char* status = amxd_object_get_value(cstring_t, object, "Status", NULL);
        if(strcmp(status, "Disabled")) {
            mqtt_client_disconnect(object);
        }
        free(status);
    }
}

void _mqtt_client_disabled(UNUSED const char* const sig_name,
                           const amxc_var_t* const data,
                           UNUSED void* const priv) {
    SAH_TRACEZ_INFO(ME, "Client disabled");
    amxd_dm_t* dm = mqtt_get_dm();
    amxd_object_t* object = amxd_dm_signal_get_object(dm, data);
    mqtt_client_t* client = object->priv;
    amxc_var_t params;

    amxc_var_init(&params);

    when_null(client, exit);
    when_not_null(client->con_retry_timer, exit);

    amxd_object_get_params(object, &params, amxd_dm_access_protected);
    when_false(GET_BOOL(&params, "Enable"), exit);
    if((!client->interface.name) || (client->interface.isup)) {
        mqtt_client_reconnect_start(object);
    }
exit:
    amxc_var_clean(&params);
}

void _mqtt_client_added(UNUSED const char* const sig_name,
                        const amxc_var_t* const data,
                        UNUSED void* const priv) {
    amxd_dm_t* dm = mqtt_get_dm();
    amxd_object_t* object = amxd_dm_signal_get_object(dm, data);

    object = amxd_object_get_instance(object, NULL, GET_UINT32(data, "index"));
    mqtt_client_auto_reconnect_configure(NULL, object, NULL);
    mqtt_client_update(object);
    mqtt_client_connect(object);
}

void _mqtt_client_auto_reconnect(UNUSED const char* const sig_name,
                                 const amxc_var_t* const data,
                                 UNUSED void* const priv) {
    amxd_dm_t* dm = mqtt_get_dm();
    amxd_object_t* object = amxd_dm_signal_get_object(dm, data);
    if(amxd_object_get_value(bool, object, "Enable", NULL)) {
        SAH_TRACEZ_INFO(ME, "MQTT.Client. parameters changed, reconnecting automatically");
        amxp_timer_t* reconnect_timer = NULL;
        amxp_timer_new(&reconnect_timer, mqtt_client_reconnect_after_ev_loop, object);

        mqtt_client_disconnect(object);
        // Reconnect after passing through event loop to make sure memory is freed
        amxp_timer_start(reconnect_timer, 1);
    }
}
void _mqtt_client_force_reconnect(UNUSED const char* const sig_name,
                                  const amxc_var_t* const data,
                                  UNUSED void* const priv) {
    amxd_dm_t* dm = mqtt_get_dm();
    amxd_object_t* object = amxd_dm_signal_get_object(dm, data);
    if(amxd_object_get_value(bool, object, "ForceReconnect", NULL)) {
        SAH_TRACEZ_WARNING(ME, "MQTT.Client.%s.ForceReconnect()",
                           amxd_object_get_name(object, 0));
        amxd_trans_t trans;
        _Client_ForceReconnect(object, NULL, NULL, NULL);
        amxd_trans_init(&trans);
        amxd_trans_select_object(&trans, object);
        amxd_trans_set_value(bool, &trans, "ForceReconnect", false);
        amxd_trans_apply(&trans, mqtt_get_dm());
        amxd_trans_clean(&trans);
    }
}

void _mqtt_client_auto_reconnect_toggle(UNUSED const char* const sig_name,
                                        UNUSED const amxc_var_t* const data,
                                        UNUSED void* const priv) {
    amxd_dm_t* dm = mqtt_get_dm();
    amxd_object_t* object = amxd_dm_signal_get_object(dm, data);
    mqtt_client_auto_reconnect_configure(NULL, object, NULL);
}
