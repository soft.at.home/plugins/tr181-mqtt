/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include <debug/sahtrace.h>

#include "dm_mqtt_utilities.h"
#include "mqtt_client_connect.h"
#include "mqtt_dns.h"

static int mqtt_client_get_reconnect_retry_time(uint32_t* reconnect_time,
                                                amxd_object_t* object) {
    int retval = -1;
    uint32_t retry_time = 0;
    uint32_t interval_multiplier = 0;
    uint32_t max_wait_interval = 0;
    uint32_t min = 0;
    uint32_t max = 0;
    mqtt_client_t* client = NULL;
    amxc_var_t params;

    when_null(object, exit);
    client = (mqtt_client_t*) object->priv;

    amxc_var_init(&params);
    amxd_object_get_params(object, &params, amxd_dm_access_protected);
    retry_time = GET_UINT32(&params, "ConnectRetryTime");
    interval_multiplier = GET_UINT32(&params, "ConnectRetryIntervalMultiplier");
    max_wait_interval = GET_UINT32(&params, "ConnectRetryMaxInterval");

    /*
       reconnect_time : [retry_time*(interval_multipler/1000)^nbr_con_retries,
                         retry_time*(interval_multiplier/1000)^nbr_con_retries+1]
     */
    min = retry_time * powf(interval_multiplier / 1000.0f, client->nbr_con_retries);
    max = retry_time * powf(interval_multiplier / 1000.0f, client->nbr_con_retries + 1);

    if(max < max_wait_interval) {
        client->nbr_con_retries++;
    } else {
        max = max_wait_interval;
    }
    if(max > min) {
        *reconnect_time = (rand() % (max - min)) + min;
    } else {
        *reconnect_time = max;
    }
    SAH_TRACEZ_INFO(ME, "Client timer values: reconnect_time %d nb_retries %d Min %d"
                    " Max %d ConnectRetryTime %d ConnectRetryIntervalMultiplier %d"
                    " ConnectRetryMaxInterval %d",
                    *reconnect_time, client->nbr_con_retries, min, max,
                    retry_time, interval_multiplier, max_wait_interval);
    retval = 0;
exit:
    amxc_var_clean(&params);
    return retval;
}

static void mqtt_client_connect_retry_cb(UNUSED amxp_timer_t* timer, void* priv) {
    amxd_object_t* object = (amxd_object_t*) priv;
    mqtt_client_t* client = (mqtt_client_t*) object->priv;
    when_null(client, exit);
    SAH_TRACEZ_INFO(ME, "Timer connect retry handler called");
    amxp_timer_delete(&client->con_retry_timer);
    client->con_retry_timer = NULL;
    mqtt_client_connect(object);
exit:
    return;
}

static void mqtt_client_disconnect_and_reconnect_cb(UNUSED amxp_timer_t* timer, void* priv) {
    amxp_timer_t* reconnect_timer = NULL;
    amxd_object_t* object = (amxd_object_t*) priv;
    mqtt_client_t* client = (mqtt_client_t*) object->priv;

    when_null(client, exit);
    SAH_TRACEZ_INFO(ME, "Timer connect retry handler called");
    amxp_timer_delete(&client->con_retry_timer);
    client->con_retry_timer = NULL;

    amxp_timer_new(&reconnect_timer, mqtt_client_reconnect_after_ev_loop, object);
    mqtt_client_disconnect(object);
    // Reconnect after passing through event loop to make sure memory is freed
    amxp_timer_start(reconnect_timer, 1);

exit:
    return;
}

void mqtt_client_reconnect_reset(amxd_object_t* object) {
    when_null(object, exit);
    mqtt_client_t* client = (mqtt_client_t*) object->priv;
    when_null(client, exit);
    SAH_TRACEZ_INFO(ME, "Reset connect retry timer");
    client->nbr_con_retries = 0;
    amxp_timer_delete(&client->con_retry_timer);
    client->con_retry_timer = NULL;
exit:
    return;
}

// This function will start a connection retry timer if there isn't one already and it will update
// the callback function of the retry timer to make sure the client is Disabled before it retries connecting
void mqtt_client_reconnect_update_cb(amxd_object_t* object) {
    mqtt_client_t* client = NULL;

    when_null(object, exit);
    client = (mqtt_client_t*) object->priv;
    when_null(client, exit);

    if(client->con_retry_timer == NULL) {
        mqtt_client_reconnect_start(object);
    }
    // mqtt_client_reconnect_start could fail (in theory) so separate NULL pointer check
    if(client->con_retry_timer != NULL) {
        client->con_retry_timer->cb = mqtt_client_disconnect_and_reconnect_cb;
    }

exit:
    return;
}

void mqtt_client_reconnect_start(amxd_object_t* object) {
    amxp_timer_t* retry_timer = NULL;
    uint32_t retry_time = 0;
    mqtt_client_t* client = NULL;

    when_null(object, exit);
    client = object->priv;
    when_null(client, exit);

    SAH_TRACEZ_INFO(ME, "Start connection retry timer");
    when_failed(mqtt_client_get_reconnect_retry_time(&retry_time, object), exit);
    amxp_timer_new(&retry_timer, mqtt_client_connect_retry_cb, object);

    // Delete existing timer if there is one
    amxp_timer_delete(&client->con_retry_timer);
    client->con_retry_timer = retry_timer;
    amxp_timer_start(retry_timer, retry_time * 1000);
exit:
    return;
}

void mqtt_client_reconnect_after_ev_loop(amxp_timer_t* timer, void* priv) {
    amxd_object_t* object = (amxd_object_t*) priv;
    mqtt_client_connect(object);
    amxp_timer_delete(&timer);
}

void mqtt_client_connect(amxd_object_t* object) {
    char* status = NULL;
    mqtt_client_t* client = NULL;
    amxc_var_t params;

    amxc_var_init(&params);

    when_null(object, exit);
    client = (mqtt_client_t*) object->priv;
    when_null(client, exit);

    SAH_TRACEZ_INFO(ME, "mqtt client connect");

    status = amxd_object_get_value(cstring_t, object, "Status", NULL);
    if((strcmp(status, "Connecting") == 0) ||
       (strcmp(status, "Connected") == 0)) {
        goto exit;
    }
    if((!client->interface.name) || (client->interface.isup)) {
        SAH_TRACEZ_INFO(ME, "Connecting client");
        bool clean_session = false;
        const char* proto_version = NULL;

        amxd_object_get_params(object, &params, amxd_dm_access_protected);

        proto_version = GET_CHAR(&params, "ProtocolVersion");
        if(strcmp(proto_version, "5.0") == 0) {
            clean_session = GET_BOOL(&params, "CleanStart");
        } else {
            clean_session = GET_BOOL(&params, "CleanSession");
        }
        if((client->con != NULL) && (client->needs_clean_start)) {
            mqtt_delete(&client->con);
            client->needs_clean_start = false; // Reset until next change of TransportProtocol
        }
        if(client->con == NULL) {
            if(mqtt_new(&(client)->con,
                        GET_CHAR(&params, "ClientID"),
                        clean_session,
                        proto_version,
                        object) != 0) {
                SAH_TRACEZ_INFO(ME, "New MQTT client is misconfigured");
                mqtt_client_update_status(object, "Error_Misconfigured");
                goto exit;
            }
        }
        if(strcmp(GET_CHAR(&params, "TransportProtocol"), "TLS") == 0) {
            SAH_TRACEZ_INFO(ME, "Setting up TLS encryption");
            if(mqtt_tls_setup(client->con, &params) != 0) {
                SAH_TRACEZ_ERROR(ME, "Could not setup TLS encryption");
                mqtt_client_update_status(object, "Error_Misconfigured");
                goto exit;
            }
        }
        mqtt_dns_resolve(object);
    }

exit:
    amxc_var_clean(&params);
    free(status);
}

void mqtt_client_disconnect(amxd_object_t* object) {
    SAH_TRACEZ_INFO(ME, "Execute client disconnect");
    mqtt_client_t* client = NULL;

    when_null(object, exit);
    client = (mqtt_client_t*) object->priv;

    if((client == NULL) || (client->con == NULL)) {
        mqtt_client_update_status(object, "Error");
        goto exit;
    }

    if(mqtt_disconnect(client->con, 0) == 0) {
        SAH_TRACEZ_INFO(ME, "Client disconnected");
        mqtt_client_update_status(object, "Disabled");
        if(client->con->misc_timer != NULL) {
            amxp_timer_delete(&client->con->misc_timer);
            client->con->misc_timer = NULL;
        }
    } else {
        SAH_TRACEZ_ERROR(ME, "Error during mqtt_disconnect");
        mqtt_client_update_status(object, "Error");
    }
exit:
    return;
}
